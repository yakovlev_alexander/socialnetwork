package com.getjavajob.training.web1608.yakovleva.ui.utils;

import com.cloudinary.Cloudinary;
import com.cloudinary.Singleton;
import com.cloudinary.utils.ObjectUtils;
import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.EntityType;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Map;

public class WebAppUtils {
    private static Cloudinary getCloudinary() {
        return Singleton.getCloudinary();
    }

    public static byte[] savePic(MultipartFile filePart, long id, EntityType type) throws IOException {
        File file = new File(createPictureName(id, type));
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(filePart.getBytes());
        }
        Map params = ObjectUtils.asMap("use_filename", true, "unique_filename", false, "invalidate", true);
        getCloudinary().uploader().upload(file, params);
        return FileUtils.readFileToByteArray(file);
    }

    private static String createPictureName(long id, EntityType type) {
        return id + "_" + type;
    }

    public static void writeProfileInSession(Account account, HttpSession session) {
        session.setAttribute("account", account);
        session.setAttribute("id", Long.toString(account.getId()));
    }

    public static String dateToStr(java.util.Date date) {
        if (date != null) {
            return new SimpleDateFormat("dd.MM.yyyy").format(date);
        } else {
            return null;
        }
    }
}
