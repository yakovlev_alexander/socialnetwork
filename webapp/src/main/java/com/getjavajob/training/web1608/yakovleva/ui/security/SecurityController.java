package com.getjavajob.training.web1608.yakovleva.ui.security;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import com.getjavajob.training.web1608.yakovleva.ui.utils.WebAppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
public class SecurityController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView("login");
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("logout", "You've been logged out successfully.");
        }
        return model;
    }

    @RequestMapping(value="/home", method = RequestMethod.GET)
    public String executeSecurity(Principal principal, HttpSession session) {
        Account account;
        if (principal != null && (account = accountService.getAccountByEmail(principal.getName())) != null) {
            WebAppUtils.writeProfileInSession(account, session);
            return "redirect:/account/" + account.getId() + "/home";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) throws ServletException {
        request.logout();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            System.out.println("try logout");
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }
}
