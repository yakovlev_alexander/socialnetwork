package com.getjavajob.training.web1608.yakovleva.ui;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.EntityType;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.common.post.GroupPost;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import com.getjavajob.training.web1608.yakovleva.service.GroupService;
import com.getjavajob.training.web1608.yakovleva.service.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.List;

import static com.getjavajob.training.web1608.yakovleva.ui.utils.WebAppUtils.*;

@Controller
public class GroupController {
    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/group/{id}/home")
    public ModelAndView accountMainPage(@PathVariable long id, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("groupHome");
        Group group = groupService.getEntityById(id);
        modelAndView.addObject("group", group);
        modelAndView.addObject("registrationDate", dateToStr(group.getRegistrationDate()));
        long sessionId = getSessionId(session);
        Account sessionAccount = accountService.getEntityById(sessionId);
        List<Group> sessionUserGroups = accountService.getGroupsOfAccount(sessionAccount);
        for (Group userGroup : sessionUserGroups) {
            if (group.equals(userGroup)) {
                modelAndView.addObject("member", true);
            }
        }
        List<GroupPost> posts = groupService.getEntityPosts(group);
        modelAndView.addObject("posts", posts);
        return modelAndView;
    }

    private long getSessionId(HttpSession session) {
        return Long.valueOf((String) session.getAttribute("id"));
    }

    @RequestMapping(value = "/group/{id}/members", method = RequestMethod.GET)
    public ModelAndView showGroups(@PathVariable long id) {
        Group group = groupService.getEntityById(id);
        List<Account> members = groupService.getGroupMembers(group);
        ModelAndView modelAndView = new ModelAndView("members");
        modelAndView.addObject("group", group);
        modelAndView.addObject("members", members);
        return modelAndView;
    }

    @RequestMapping(value = "/joinGroup", method = RequestMethod.GET)
    public String joinGroup(@RequestParam long groupId, HttpSession session) {
        groupService.addMember(groupId, getSessionId(session));
        return "redirect:/group/" + groupId + "/home";
    }

    @RequestMapping(value = "/groupSettings", method = RequestMethod.GET)
    public ModelAndView groupSettings(@RequestParam(required = false) Long groupId) {
        ModelAndView modelAndView = new ModelAndView("groupSettings");
        if (groupId != null) {
            modelAndView.addObject("group", groupService.getEntityById(groupId));
        }
        return modelAndView;
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST)
    public String createGroup(@ModelAttribute Group newGroup, HttpSession session,
                              @RequestParam(required = false, value = "file") MultipartFile filePart,
                              final RedirectAttributes redirectAttr) throws ServiceException, IOException {
        Account sessionAccount = (Account) session.getAttribute("account");
        Group oldGroup = groupService.getGroupByName(newGroup.getGroupName());
        if (oldGroup == null) {
            newGroup.setCreator(sessionAccount);
            newGroup = groupService.createEntity(newGroup);
            if (filePart != null && filePart.getSize() != 0) {
                newGroup.setPicture(savePic(filePart, newGroup.getId(), EntityType.GROUP));
            }
            groupService.updateEntity(newGroup);
            return "redirect:/group/" + newGroup.getId() + "/home";
        } else {
            redirectAttr.addAttribute("error", "The group with the name " + newGroup.getGroupName() + " already exists");
            return "redirect:groupSettings";
        }
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.POST)
    public String updateGroup(@ModelAttribute Group newGroup, @RequestParam long groupId, HttpSession session,
                              @RequestParam(required = false, value = "file") MultipartFile filePart,
                              final RedirectAttributes redirectAttr) throws ServiceException, IOException {
        long sessionId = getSessionId(session);
        Account sessionAccount = accountService.getEntityById(sessionId);
        Group oldGroup = groupService.getEntityById(groupId);
        if (oldGroup.getCreator().equals(sessionAccount)) {
            if (groupService.getGroupByName(newGroup.getGroupName()) == null || newGroup.getGroupName().equals(oldGroup.getGroupName())) {
                oldGroup.setGroupName(newGroup.getGroupName());
                oldGroup.setAddInfo(newGroup.getAddInfo());
                if (filePart != null && filePart.getSize() != 0) {
                    oldGroup.setPicture(savePic(filePart, oldGroup.getId(), EntityType.GROUP));
                }
                groupService.updateEntity(oldGroup);
            } else {
                redirectAttr.addAttribute("error", "The group with the name " + newGroup.getGroupName() + " already exists");
                return "redirect:groupSettings?groupId=" + groupId;
            }
        }
        return "redirect:group/" + groupId + "/home";
    }
}
