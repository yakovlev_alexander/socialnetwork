package com.getjavajob.training.web1608.yakovleva.ui;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.common.post.AccountPost;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import com.getjavajob.training.web1608.yakovleva.service.ServiceException;
import com.getjavajob.training.web1608.yakovleva.ui.utils.WebAppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import java.util.Date;
import java.util.List;

import static com.getjavajob.training.web1608.yakovleva.common.utils.CommonUtils.getHashPassword;
import static com.getjavajob.training.web1608.yakovleva.ui.utils.WebAppUtils.dateToStr;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private static final Logger errLogger = LoggerFactory.getLogger("ErrorLogger");

    @Autowired
    private AccountService accountService;

    @RequestMapping("registration")
    public String goRegistration() {
        return "registration";
    }

    @RequestMapping(value = "do-register", method = RequestMethod.POST)
    public String doRegister(@ModelAttribute Account account,
                             @RequestParam("password2") String approvedPassword,
                             HttpSession session) throws ServiceException {
        String email = account.getEmail();
        String pass = account.getPass();
        String familyName = account.getFamilyName();
        String name = account.getName();
        String middleName = account.getMiddleName();
        if (!pass.equals(approvedPassword)) {
            errLogger.error("Password doesn't match on settings page");
            session.setAttribute("reason", "Password doesn't match");
            return "redirect:registration";
        }

        if (accountService.getAccountByEmail(email) == null) {
            Account newAccount = accountService.createEntity(new Account(name, familyName, email, getHashPassword(pass)));
            newAccount.setMiddleName(middleName);
            WebAppUtils.writeProfileInSession(newAccount, session);
            return "redirect:/account/" + newAccount.getId() + "/home";
        } else { // Email is busy
            session.setAttribute("reason", "The email occupied!");
            return "redirect:registration";
        }
    }

    @RequestMapping(value = "/account/{id}/friends", method = RequestMethod.GET)
    public ModelAndView showFriends(@PathVariable long id) {
        Account account = accountService.getEntityById(id);
        List<Account> friendsList = accountService.getFriendsList(account);
        List<Account> followedList = accountService.getFollowedList(account);
        List<Account> followersList = accountService.getFollowersList(account);
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("account", account);
        modelAndView.addObject("friendsList", friendsList);
        modelAndView.addObject("followedList", followedList);
        modelAndView.addObject("followersList", followersList);
        return modelAndView;
    }

    @RequestMapping(value = "/account/{id}/groups", method = RequestMethod.GET)
    public ModelAndView showGroups(@PathVariable long id) {
        Account account = accountService.getEntityById(id);
        List<Group> groups = accountService.getGroupsOfAccount(account);
        ModelAndView modelAndView = new ModelAndView("groups");
        modelAndView.addObject("account", account);
        modelAndView.addObject("groups", groups);
        return modelAndView;
    }

    @RequestMapping(value = "/account/{id}/home")
    public ModelAndView accountMainPage(@PathVariable long id, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("home");
        Account account = accountService.getEntityById(id);
        modelAndView.addObject("account", account);
        Date birthday = account.getBirthday();
        if (birthday != null) {
            modelAndView.addObject("birthday", dateToStr(birthday));
        }
        modelAndView.addObject("registrationDate", dateToStr(account.getRegistrationDate()));

        long sessionId = Long.valueOf((String) session.getAttribute("id"));
        modelAndView.addObject("relation", accountService.getRelation(sessionId, id).name());
        List<AccountPost> posts = accountService.getEntityPosts(account);
        modelAndView.addObject("posts", posts);
        logger.trace("Go to user " + account.getName() + " " + account.getFamilyName() + " home page");
        return modelAndView;
    }
}
