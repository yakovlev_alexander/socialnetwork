package com.getjavajob.training.web1608.yakovleva.ui;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.EntityType;
import com.getjavajob.training.web1608.yakovleva.common.utils.CommonUtils;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.*;

import static com.getjavajob.training.web1608.yakovleva.ui.utils.WebAppUtils.*;

@Controller
public class SettingsController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "settings")
    public ModelAndView goSettings(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("settingsPage");
        Account account = accountService.getEntityById(Long.parseLong((String) session.getAttribute("id")));
        modelAndView.addObject("account", account);
        modelAndView.addObject("skills", accountService.getSkills(account));
        return modelAndView;
    }

    @RequestMapping(value = "setSettings", method = RequestMethod.POST)
    public String setSettings(@ModelAttribute Account newAccount,
                              @RequestParam(required = false, name = "old_password") String oldPass,
                              @RequestParam(required = false, value = "new_password") String newPass,
                              @RequestParam(required = false, value = "new_password_again") String newPassAgain,
                              @RequestParam(required = false, value = "file") MultipartFile filePart,
                              HttpSession session) throws IOException {
        Long accountId = Long.valueOf((String) session.getAttribute("id"));
        Account oldAccount = accountService.getEntityById(accountId);
        if (oldPass == null || !oldAccount.getPass().equals(CommonUtils.getHashPassword(oldPass))) {
            return "redirect:settings";
        } else {
            //Required fields
            oldAccount.setName(newAccount.getName());
            oldAccount.setFamilyName(newAccount.getFamilyName());
            oldAccount.setMiddleName(newAccount.getMiddleName());
            oldAccount.setBirthday(newAccount.getBirthday());
            oldAccount.setEmail(newAccount.getEmail());
            //Nullable fields
            String personalPhone = newAccount.getPersonalPhone();
            oldAccount.setPersonalPhone(personalPhone.isEmpty() ? null : personalPhone);
            String workPhone = newAccount.getWorkPhone();
            oldAccount.setWorkPhone(workPhone.isEmpty() ? null : workPhone);
            String icq = newAccount.getIcq();
            oldAccount.setIcq(icq.isEmpty() ? null : icq);
            String skype = newAccount.getSkype();
            oldAccount.setSkype(skype.isEmpty() ? null : skype);
            String addInfo = newAccount.getAddInfo();
            oldAccount.setAddInfo(addInfo.isEmpty() ? null : addInfo);

            if (newPass != null && !newPass.isEmpty() && newPass.equals(newPassAgain)) {
                oldAccount.setPass(CommonUtils.getHashPassword(newPass));
            }
            if (filePart != null && filePart.getSize() != 0) {
                oldAccount.setPicture(savePic(filePart, oldAccount.getId(), EntityType.ACCOUNT));
            }
            oldAccount.setSkills(newAccount.getSkills());
            accountService.updateEntity(oldAccount);
            writeProfileInSession(oldAccount, session);
            return "redirect:account/" + oldAccount.getId() + "/home";
        }
    }
}
