package com.getjavajob.training.web1608.yakovleva.ui;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import com.getjavajob.training.web1608.yakovleva.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class PostController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/{type}/{entityId}/sendPost", method = RequestMethod.POST)
    public String sendPost(@RequestParam String message, @PathVariable String type,
                           @PathVariable long entityId, HttpSession session) {
        Account from = accountService.getEntityById(Long.valueOf((String) session.getAttribute("id")));
        if (type.equals("account")) {
            Account to = accountService.getEntityById(entityId);
            accountService.sendPost(from, to, message);
        } else if (type.equals("group")) {
            Group to = groupService.getEntityById(entityId);
            groupService.sendPost(from,to,message);
        }
        return "redirect:/" + type + "/" + entityId + "/home";
    }

    @RequestMapping(value = "/{type}/{entityId}/deletePost", method = RequestMethod.GET)
    public String deletePost(@RequestParam long id, @PathVariable String type, @PathVariable long entityId) {
        if (type.equals("account")) {
            accountService.deletePost(id);
        } else if (type.equals("group")) {
            groupService.deletePost(id);
        }
        return "redirect:/" + type + "/" + entityId + "/home";
    }
}
