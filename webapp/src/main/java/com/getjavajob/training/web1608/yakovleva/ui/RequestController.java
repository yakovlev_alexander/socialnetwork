package com.getjavajob.training.web1608.yakovleva.ui;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("id")
public class RequestController {
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/sendFriendRequest", method = RequestMethod.GET)
    public ModelAndView sendFriendRequest(@ModelAttribute("id") long fromId, @RequestParam long toId) {
        Account from = accountService.getEntityById(fromId);
        Account to = accountService.getEntityById(toId);
        accountService.sendFriendRequest(from, to);
        return new ModelAndView("redirect:/account/" + toId + "/home");
    }

    @RequestMapping(value = "/acceptFriendRequest", method = RequestMethod.GET)
    public String acceptFriendRequest(@ModelAttribute("id") long toId, @RequestParam long fromId) {
        Account from = accountService.getEntityById(fromId);
        Account to = accountService.getEntityById(toId);
        accountService.acceptFriendRequest(from, to);
        return "redirect:/account/" + fromId + "/home";
    }

    @RequestMapping(value = "/deleteRequest", method = RequestMethod.GET)
    public String deleteRequest(@ModelAttribute("id") long sessionId, @RequestParam("accId") long accountId) {
        Account from = accountService.getEntityById(sessionId);
        Account to = accountService.getEntityById(accountId);
        accountService.removeFriend(from, to);
        return "redirect:/account/" + accountId + "/home";
    }
}
