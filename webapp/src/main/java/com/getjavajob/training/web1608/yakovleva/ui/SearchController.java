package com.getjavajob.training.web1608.yakovleva.ui;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.EntityType;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.service.AbstractService;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import com.getjavajob.training.web1608.yakovleva.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.LinkedList;
import java.util.List;

@Controller
public class SearchController {
    private static int PAGE_SIZE = 4;

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "show_search", method = RequestMethod.GET)
    public ModelAndView showSearch(@RequestParam("search-by-pattern") String pattern) {
        ModelAndView modelAndView = new ModelAndView("searchResults");
        modelAndView.addObject("pattern", pattern);
        modelAndView.addObject("accountPages", getPages(pattern, accountService));
        modelAndView.addObject("groupPages", getPages(pattern, groupService));
        modelAndView.addObject("accounts", getAccountSearchEntities(pattern, 0, PAGE_SIZE));
        modelAndView.addObject("groups", getGroupSearchEntities(pattern, 0, PAGE_SIZE));
        return modelAndView;
    }

    private static long getPages(String pattern, AbstractService service) {
        long totalRows = service.getSearchEntitiesCount(pattern);
        return (long) Math.ceil ((double) totalRows / PAGE_SIZE);
    }

    @ResponseBody
    @RequestMapping(value = "/autocomplete", method = RequestMethod.GET)
    public List<SearchEntity> autocomplete(@RequestParam("pattern") final String pattern) {
        List<SearchEntity> entities = getAccountSearchEntities(pattern, 0, 10);
        int accountsCount = entities.size();
        if (accountsCount < 10) {
            entities.addAll(getGroupSearchEntities(pattern, 0, 10 - accountsCount));
        }
        return entities;
    }

    @ResponseBody
    @RequestMapping(value = "/getPage", method = RequestMethod.GET)
    public List<SearchEntity> getAccountPage(@RequestParam int page, @RequestParam String pattern, @RequestParam String type) {
        if (type.equals("account")) {
            return getAccountSearchEntities(pattern, (page - 1) * PAGE_SIZE, PAGE_SIZE);
        } if (type.equals("group")) {
            return getGroupSearchEntities(pattern, (page - 1) * PAGE_SIZE, PAGE_SIZE);
        }
        return null;
    }

    private List<SearchEntity> getAccountSearchEntities(String pattern, int start, int count) {
        return getAccountSearchEntities(accountService.getEntitiesByPattern(pattern, start, count));
    }

    private List<SearchEntity> getAccountSearchEntities(List<Account> accounts) {
        List<SearchEntity> entities = new LinkedList<>();
        for (Account account : accounts) {
            String fullAccountName = getFullAccountName(account);
            entities.add(new SearchEntity(fullAccountName, account.getEmail(), EntityType.ACCOUNT, account.getId(), account.getPicture() != null));
        }
        return entities;
    }

    private static String getFullAccountName(Account account) {
        return account.getName() + " " + account.getFamilyName();
    }

    private List<SearchEntity> getGroupSearchEntities(String pattern, int start, int count) {
        return getGroupSearchEntities(groupService.getEntitiesByPattern(pattern, start, count));
    }

    private List<SearchEntity> getGroupSearchEntities(List<Group> groups) {
        List<SearchEntity> entities = new LinkedList<>();
        for (Group group : groups) {
            String secondaryName = getFullAccountName(groupService.getGroupCreator(group));
            entities.add(new SearchEntity(group.getGroupName(), "by " + secondaryName, EntityType.GROUP, group.getId(), group.getPicture() != null));
        }
        return entities;
    }

    public static class SearchEntity {
        private String primaryName;
        private String secondaryName;
        private EntityType type;
        private long id;
        private boolean hasPic;

        public SearchEntity(String primaryName, String secondaryName, EntityType type, long id, boolean hasPic) {
            this.primaryName = primaryName;
            this.secondaryName = secondaryName;
            this.type = type;
            this.id = id;
            this.hasPic = hasPic;
        }

        public String getPrimaryName() {
            return primaryName;
        }

        public String getSecondaryName() {
            return secondaryName;
        }

        public EntityType getType() {
            return type;
        }

        public long getId() {
            return id;
        }

        public boolean isHasPic() {
            return hasPic;
        }

        @Override
        public String toString() {
            return "SearchEntity{" +
                    "primaryName='" + primaryName + '\'' +
                    ", secondaryName='" + secondaryName + '\'' +
                    ", type='" + type + '\'' +
                    ", id=" + id +
                    '}';
        }
    }
}
