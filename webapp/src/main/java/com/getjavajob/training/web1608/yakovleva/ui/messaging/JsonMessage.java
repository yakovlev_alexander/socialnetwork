package com.getjavajob.training.web1608.yakovleva.ui.messaging;

public class JsonMessage {
    private String fromId;
    private String toId;
    private String text;


    public JsonMessage() {
    }

    public JsonMessage(String fromId, String toId, String text) {
        this.fromId = fromId;
        this.toId = toId;
        this.text = text;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
