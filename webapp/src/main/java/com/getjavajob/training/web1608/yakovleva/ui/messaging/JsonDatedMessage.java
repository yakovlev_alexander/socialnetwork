package com.getjavajob.training.web1608.yakovleva.ui.messaging;

public class JsonDatedMessage extends JsonMessage {
    private String date;

    public JsonDatedMessage() {
    }

    public JsonDatedMessage(String fromId, String toId, String text, String date) {
        super(fromId, toId, text);
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
