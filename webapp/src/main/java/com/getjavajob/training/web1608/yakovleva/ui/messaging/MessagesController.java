package com.getjavajob.training.web1608.yakovleva.ui.messaging;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.PrivateMsg;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class MessagesController {
    @Autowired
    private AccountService accountService;

    @MessageMapping("/chat/{from}/{to}")
    @SendTo("/topic/{from}/{to}")
    public JsonDatedMessage send(JsonMessage message) throws Exception {
        accountService.sendMessage(Long.valueOf(message.getFromId()), Long.valueOf(message.getToId()), message.getText());
        String date = new SimpleDateFormat("HH:mm:ss dd:MM:yyyy").format(new Date());
        return new JsonDatedMessage(message.getFromId(), message.getToId(), message.getText(), date);
    }

    @RequestMapping(value = "messages", method = RequestMethod.GET)
    public ModelAndView goMessages(HttpSession session, @RequestParam(required = false) Long recipientId) {
        ModelAndView modelAndView = new ModelAndView("myMessages");
        Account sessionAccount = getAccountFromSession(session);
        List<Account> accountList = getListOfRecipientsForAccount(sessionAccount);
        Account recipient = null;
        if (recipientId != null) {
            recipient = accountService.getEntityById(recipientId);
            if (!accountList.contains(recipient)) {
                accountList.add(recipient);
            }
        } else {
            if (accountList.size() > 0) {
                recipient = accountList.get(0);
            }
        }
        modelAndView.addObject("accounts", accountList);
        modelAndView.addObject("recipient", recipient);
        if (recipient != null) {
            modelAndView.addObject("messages", accountService.getMessagesOfAccounts(sessionAccount, recipient));
        }
        return modelAndView;
    }

    private List<Account> getListOfRecipientsForAccount(Account account) {
        List<PrivateMsg> msgs = accountService.getAccountMessages(account);
        Set<Account> accountsSet = new LinkedHashSet<>();
        for (PrivateMsg msg : msgs) {
            Account fromAccount = msg.getFromAccount();
            Account toAccount = msg.getToAccount();
            if (!fromAccount.equals(account)) {
                accountsSet.add(fromAccount);
            } else if (!toAccount.equals(account)) {
                accountsSet.add(toAccount);
            }
        }
        return new LinkedList<>(accountsSet);
    }

    private Account getAccountFromSession(HttpSession session) {
        return accountService.getEntityById(Long.valueOf((String) session.getAttribute("id")));
    }
}
