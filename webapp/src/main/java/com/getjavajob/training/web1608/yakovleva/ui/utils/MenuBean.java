package com.getjavajob.training.web1608.yakovleva.ui.utils;

import java.io.Serializable;

public class MenuBean implements Serializable {
    private String home;
    private String friends;
    private String messages;
    private String groups;
    private String settings;

    public MenuBean() {
        makeFieldsInactive();
    }

    private void makeFieldsInactive() {
        home = "inactive";
        friends = "inactive";
        messages = "inactive";
        groups = "inactive";
        settings = "inactive";
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        makeFieldsInactive();
        this.home = home;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        makeFieldsInactive();
        this.friends = friends;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        makeFieldsInactive();
        this.messages = messages;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        makeFieldsInactive();
        this.groups = groups;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        makeFieldsInactive();
        this.settings = settings;
    }
}
