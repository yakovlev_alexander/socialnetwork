package com.getjavajob.training.web1608.yakovleva.ui;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.service.AccountService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;

@Controller
public class XMLController {
    private static final Logger logger = LoggerFactory.getLogger("ErrorLogger");

    @Autowired
    private AccountService accountService;

    @ResponseBody
    @RequestMapping(value = "makeXML", method = RequestMethod.GET)
    public HttpEntity<byte[]> makeXML(@RequestParam long id) {
        Account account = accountService.getEntityById(id);
        XStream xStream = getXStream();
        byte[] xml = xStream.toXML(account).getBytes();

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "xml"));
        header.set("Content-Disposition", "inline; filename=" + "Account" + id + ".xml");
        header.setContentLength(xml.length);
        return new HttpEntity<>(xml, header);
    }

    private XStream getXStream() {
        XStream xStream = new XStream(new StaxDriver());
        xStream.processAnnotations(Account.class);
        return xStream;
    }

    @RequestMapping(value = "readXML", method = RequestMethod.POST)
    public ModelAndView readXML(@RequestParam("xmlFile") MultipartFile xmlFile,
                                final RedirectAttributes redirectAttr) throws IOException, SAXException {
        ModelAndView modelAndView = new ModelAndView("settingsPage");
        if (xmlFile != null && !xmlFile.isEmpty()) {
            byte[] bytes = IOUtils.toByteArray(xmlFile.getInputStream()); // for reusing InputStream bytes
            try {
                validateXML(new ByteArrayInputStream(bytes)); // Validating xml file
                StringBuilder sbXML = new StringBuilder();
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bytes)))) {
                    while (reader.ready()) {
                        sbXML.append(reader.readLine());
                    }
                }
                XStream xStream = getXStream();
                Account account = (Account) xStream.fromXML(sbXML.toString());
                modelAndView.addObject("account", account);
                modelAndView.addObject("skills", account.getSkills());
            } catch (SAXException e) {
                logger.error("Invalid xml file");
                redirectAttr.addAttribute("xmlError", "Invalid xml file");
                return new ModelAndView("redirect:/settings");
            }
        }
        return modelAndView;
    }

    private static void validateXML(InputStream xmlInputStream) throws SAXException, IOException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source xsd = new StreamSource(XMLController.class.getClassLoader().getResourceAsStream("xml-validation.xsd"));
        Schema schema = factory.newSchema(xsd);
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(xmlInputStream));
    }
}
