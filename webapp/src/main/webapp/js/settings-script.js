$(document).ready(function () {
    var maxFields = 10; //maximum skills boxes allowed
    var skillsWrapper = $(".input_skills_wrap"); //Fields skillsWrapper
    var addButton = $(".add_skill_button"); //Add button class
    var saveSettings = $(".settings_save_button");
    var appendText = '' +
        '<div><input class="form-control" name="skills" placeholder="Add skill" type="text"/>' +
        '<div class="input-group-btn">' +
        '<a type="button" class="remove_skill_button btn btn-danger">' +
        '<span class="glyphicon glyphicon-minus"></span>' +
        '</a></div><br></div>';

    var x = 1; //initlal text box count
    $(addButton).click(function (e) { //on add input button click
        e.preventDefault();
        if (x < maxFields) { //max input box allowed
            x++; //text box increment
            $(skillsWrapper).append(appendText); //add input box
        }
    });

    $(skillsWrapper).on("click", ".remove_skill_button", function (e) { //user click on remove skill
        e.preventDefault();
        $(this).parent('div').parent('div').remove();
        x--;
    });

    var currentForm;
    $(function () {
        $("#dialog-confirm").dialog({
            resizable: false,
            height: 140,
            modal: true,
            autoOpen: false,
            buttons: {
                'Save settings': function () {
                    $(this).dialog('close');
                    currentForm.submit();
                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        }).position({
            my: "center",
            at: "center",
            of: window
        });
    });

    $(saveSettings).click(function () {
        currentForm = $(this).closest('form'); // Set form to submit from dialog
        var skills = document.getElementsByName("skills_list");
        for (var i = 0; i < skills.length; i++) {
            console.log("skill.valueId" + skills[i].valueId);
            if (!validate(skills[i].valueId)) {
                return false;
            }
        }
        $("#dialog-confirm").dialog('open');
        return false;
    });

    function validate(arg) {
        var reg = /^\d|[()%*&!^]/g;
        if (arg.search(reg) >= 0 || arg.length == 0) {
            alert(arg + " skill is invalid (empty or starts with number or include ()%*&!^)");
            return false;
        } else {
            return true;
        }
    }
});
