$(document).ready(
    function () {
        $("#search-id").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: window.location.origin + '/autocomplete',
                    data: {
                        pattern: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (entry, i) {
                            return {
                                valueId: entry.id,
                                valueType: entry.type.toLowerCase(),
                                label: entry.primaryName + ' : ' + entry.type.toLowerCase()
                            }
                        }));
                    }
                });
            },
            select: function (event, ui) {
                location.pathname = "../" + ui.item.valueType + "/" + ui.item.valueId + "/home";
            },
            minLength: 1
        });
    }
);
$(document).ready(
    $(".page").click(function () {
        var pageNum = $(this).find('a').attr('name');
        var pattern = document.getElementById("pattern").innerHTML;
        var type = $(this).attr('type');
        $.cloudinary.config({
            cloud_name: "alexyakovlev90",
            api_key: "924778954634576",
            secure: true,
            invalidate: true,
            width: 150,
            height: 150
        });
        $.ajax({
            url: window.location.origin + '/getPage',//the script to call to get data
            data: "page=" + pageNum + "&pattern=" + pattern + '&type=' + type, //you can insert url argumnets here to pass to /getPage for example "id=5"
            dataType: 'json',                //data format
            success: function (data)          //on receive of reply
            {
                $('#' + type + '-search-results').empty();
                $.each(data, function (index, entity) {
                    var photoURL;
                    if (entity.hasPic) {
                        photoURL = $.cloudinary.url(entity.id + "_" + entity.type.toLowerCase(), null);
                    } else {
                        photoURL = window.location.origin + '/img/' + entity.type.toLowerCase() + '-default.jpg';
                    }
                    var profileURL = window.location.origin + '/' + type + '/' + entity.id + '/home';
                    $('#' + type + '-search-results').append(
                        '<div class="col-sm-3"><div class="card">'
                        + '<canvas class="header-bg" width="250" height="70"></canvas>'
                        + '<div class="avatar"><img src="' + photoURL + '"/></div>'
                        + '<div class="content"><p>' + entity.primaryName + '<br>' + entity.secondaryName + '</p>'
                        + '<p><a type="button" class="btn btn-default" href="' + profileURL + '">Show profile</a></p>'
                        + '</div></div></div>'
                    );
                });
            }
        });
    })
);