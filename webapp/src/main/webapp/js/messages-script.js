var stompClient = null;

$(document).ready(
    function () {
        if ($("#msgContainer").length) {
            var from = $("#msgFromId").text();
            var to = $("#msgToId").text();

            function connect(from, to) {
                disconnect();
                var socket = new SockJS('/chat');
                stompClient = Stomp.over(socket);
                stompClient.connect({}, function (frame) {
                    console.log('Connected: ' + frame);
                    $("#sendMsg").removeAttr('disabled');
                    stompClient.subscribe('/topic' + getChatRoom(from, to), function (message) {
                        showMessageOutput(JSON.parse(message.body));
                    });
                });
            }

            function disconnect() {
                if (stompClient != null) {
                    stompClient.disconnect();
                }
                console.log("Disconnected");
            }

            function commonMsg(text, date) {
                return '<p>' + text + '</p>' +
                    '<small class="form-text text-muted">' + date + '</small>' +
                    '</div>';
            }

            function getRightMsg(text, date) {
                return '<div class="well msg right-msg">' + commonMsg(text, date)
            }

            function getLeftMsg(text, date) {
                return '<div class="well msg left-msg">' + commonMsg(text, date)
            }

            function showMessageOutput(messageOutput) {
                if (messageOutput.fromId == from) {
                    $('#messages-list').append(getRightMsg(messageOutput.text, messageOutput.date));
                } else {
                    $('#messages-list').append(getLeftMsg(messageOutput.text, messageOutput.date));
                }
            }

            connect(from, to);
        }
    }
);

function getChatRoom(from, to) {
    return from < to ? '/' + from + '/' + to : '/' + to + '/' + from;
}

$(document).ready(
    $("#sendMsg").click(function () {
        function sendMessage() {
            var from = $("#msgFromId").text();
            var to = $("#msgToId").text();
            var text = $("#textMessage").val();
            stompClient.send("/app/chat" + getChatRoom(from, to), {},
                JSON.stringify({'fromId': from, 'toId': to, 'text': text}));
        }
        sendMessage();
    })
);
