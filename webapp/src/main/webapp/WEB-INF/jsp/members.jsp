<%@include file="common/header.jsp" %>

<jsp:setProperty name="MenuBean" property="friends" value="active"/>
<%@include file="common/MenuPanel.jsp" %>

<div class="container">
    <div class="row">
        <h1>
            ${group.groupName} group members
        </h1>
    </div>
    <div class="row">
        <c:forEach var="account" items="${members}">

            <%@include file="common/accountCard.jsp"%>

        </c:forEach>
    </div>
</div>

<%@include file="common/footer.jsp" %>

