<%@include file="common/header.jsp" %>

<%@include file="common/MenuPanel.jsp" %>

<div class="container">
    <div class="row">
        <h1>
            <c:choose>
                <c:when test="${group == null}">
                    Create new Group
                </c:when>
                <c:otherwise>
                    Update Group
                </c:otherwise>
            </c:choose>
        </h1>
    </div>
    <div class="row">
        <form:form class="form-horizontal" method="POST"
                   action='${pageContext.request.contextPath}/${group == null ? "createGroup" : "updateGroup?groupId=".concat(group.id)}'
                   enctype="multipart/form-data">

            <div class="form-group required">
                <label class="col-md-2 control-label">Group name</label>
                <div class="col-md-4">
                    <input class="form-control" maxlength="30" name="groupName" required="required"
                           placeholder="Group name" value='${group != null ? group.groupName : ""}'
                           type="text"/>
                </div>
            </div>

            <div class="form-group required">
                <label class="col-md-2 control-label">Group description</label>
                <div class="col-md-4">
                    <textarea class="form-control" name="addInfo" rows="3" required="required">${group != null ? group.addInfo : ""}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="groupLogo" class="col-md-2 control-label">Upload group logo</label>
                <div class="col-md-4">
                    <input type="file" class="form-control-file" id="groupLogo" name="file"
                           aria-describedby="fileHelp"/>
                    <small id="fileHelp" class="form-text text-muted">
                        Please, choose the file with image extension
                    </small>
                </div>
            </div>

            <c:if test='${param.error != null}'>
                <div class="col-md-2"></div>
                <div class="row alert alert-danger col-md-4">
                    <c:out value='${param.error}'/>
                </div>
            </c:if>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">
                        <c:choose>
                            <c:when test="${group == null}">
                                Create Group
                            </c:when>
                            <c:otherwise>
                                Update Group
                            </c:otherwise>
                        </c:choose>
                    </button>
                </div>
            </div>

        </form:form>
    </div>
</div>
<%@include file="common/footer.jsp" %>
