<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@include file="common/header.jsp" %>
<jsp:setProperty name="MenuBean" property="settings" value="active"/>

<%@include file="common/MenuPanel.jsp" %>

<div class="container">
    <div class="row">
        <h1>
            Change profile information
        </h1>
    </div>
    <div id="dialog-confirm" title="Are you sure about that?" style="visibility: hidden"></div>
    <div class="row">
        <form:form class="form-horizontal" method="POST"
                   th:action="${pageContext.request.contextPath}/readXML?${_csrf.parameterName}=${_csrf.token}"
                   enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-md-2 control-label">Import data:</label>
                <div class="col-md-3">
                    <input type="file" class="form-control-file" name="xmlFile" id="input-xml"
                           aria-describedby="xmlHelp">
                    <small id="xmlHelp" class="form-text text-muted">
                        Please, choose the file with *.xml extension
                    </small>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-primary">Use XML</button>
                </div>
            </div>
            <c:if test='${param.xmlError != null}'>
                <div class="col-md-2"></div>
                <div class="row alert alert-danger col-md-4">
                    <c:out value='${param.xmlError}'/>
                </div>
            </c:if>
        </form:form>
    </div>

    <div class="row">
        <br/>
    </div>

    <form:form class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/setSettings"
               enctype="multipart/form-data">
        <div class="form-group">
            <label for="input-image" class="col-md-2 control-label">Upload profile image</label>
            <div class="col-md-4">
                <input type="file" class="form-control-file" id="input-image" name="file" aria-describedby="fileHelp"/>
                <small id="fileHelp" class="form-text text-muted">
                    Please, choose the file with image extension
                </small>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">First name</label>
            <div class="col-md-4">
                <input class="form-control" maxlength="30" name="name" value="${account.name}" placeholder="First name"
                       type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Middle name</label>
            <div class="col-md-4">
                <input class="form-control" maxlength="30" name="middleName" value="${account.middleName}"
                       placeholder="Middle name" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Family name</label>
            <div class="col-md-4">
                <input class="form-control" maxlength="30" name="familyName" value="${account.familyName}"
                       placeholder="Family name" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Birthday</label>
            <div class="col-md-4">
                <input type="date" class="form-control" id="birthday" name="birthday" value="${account.birthday}"
                       placeholder="Birthday">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Personal phone</label>
            <div class="col-md-4">
                <input class="form-control" maxlength="16" name="personalPhone" value="${account.personalPhone}"
                       placeholder="Personal phone"
                       type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Work phone</label>
            <div class="col-md-4">
                <input class="form-control" maxlength="16" name="workPhone" value="${account.workPhone}"
                       placeholder="Work phone" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Email</label>
            <div class="col-md-4">
                <input class="form-control" name="email" placeholder="email" value="${account.email}" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">ICQ</label>
            <div class="col-md-4">
                <input class="form-control" maxlength="9" name="icq" value="${account.icq}" placeholder="icq"
                       type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Skype</label>
            <div class="col-md-4">
                <input class="form-control" name="skype" value="${account.skype}" placeholder="Skype" type="text"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">Additional information</label>
            <div class="col-md-4">
                <textarea class="form-control" name="addInfo" rows="3">${account.addInfo}</textarea>
            </div>
        </div>
        <%--ADD SKILLS (JS SUPPORTED)--%>
        <div class="form-group">
            <label class="col-md-2 control-label">Skills</label>
            <div class="col-md-4 form-inline">
                <div class="input_skills_wrap input-group">
                    <div>
                        <a class="add_skill_button btn btn-success">Add More Skills</a>
                    </div>
                    <br>
                    <c:forEach var='skill' varStatus="skillStatus" items='${skills}'>
                        <div>
                            <input class="form-control" name="skills" value="${skill}" placeholder="Add skill"
                                   type="text"/>
                            <div class="input-group-btn">
                                <a type="button" class="remove_skill_button btn btn-danger">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </a>
                            </div>
                            <br>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <%--ADD SKILLS (END)--%>

        <div class="form-group">
            <label class="col-md-2 control-label">New password</label>
            <div class="col-md-4">
                <input class="form-control" name="new_password" placeholder="New password" type="password"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">New password (again)</label>
            <div class="col-md-4">
                <input class="form-control" name="new_password_again" placeholder="New password (again)"
                       type="password"/>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-md-2 control-label">Old password</label>
            <div class="col-md-4">
                <input class="form-control" name="old_password" placeholder="Old password" required="required"
                       type="password"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="settings_save_button btn btn-primary">Change information</button>
            </div>
        </div>
    </form:form>
</div>
<%@include file="common/footer.jsp" %>
