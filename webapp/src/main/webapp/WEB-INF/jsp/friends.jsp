<%@include file="common/header.jsp" %>

<jsp:setProperty name="MenuBean" property="friends" value="active"/>
<%@include file="common/MenuPanel.jsp" %>

<div class="container">
    <div class="row">
        <h1>
            ${account.name} ${account.familyName} friends
        </h1>
    </div>
    <div class="row">
        <c:forEach var="account" items="${friendsList}">
            <%@include file="common/accountCard.jsp"%>
        </c:forEach>
    </div>
    <div class="row">
        <h2>
            Followed accounts
        </h2>
    </div>
    <div class="row">
    <c:forEach var="account" items="${followedList}">
        <%@include file="common/accountCard.jsp"%>
    </c:forEach>
    </div>
    <div class="row">
        <h2>
            Followers
        </h2>
    </div>
    <div class="row">
    <c:forEach var="account" items="${followersList}">
        <%@include file="common/accountCard.jsp"%>
    </c:forEach>
    </div>
</div>

<%@include file="common/footer.jsp" %>
