<%@include file="common/header.jsp" %>

<jsp:setProperty name="MenuBean" property="messages" value="active"/>
<%@include file="common/MenuPanel.jsp" %>

<div class="container" id="msgContainer">
    <div class="row">
        <h1>
            My messages
        </h1>
    </div>
    <div hidden id="msgFromId">${sessionScope.get("id")}</div>
    <div hidden id="msgToId">${recipient.id}</div>
    <div class="row">
        <div class="col-md-3">
            <div class="page-sidebar">
                <div class="sidebar-buttons">
                    <c:forEach var="account" items="${accounts}">
                        <p>
                            <a class="messageAccount btn btn-default ${account.id == recipient.id ? "active" : ""}"
                               name="${account.id}"
                               href="${pageContext.request.contextPath}/messages?recipientId=${account.id}">
                                    ${account.familyName} ${account.name}
                            </a>
                        </p>
                    </c:forEach>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div id="messages-list">
                <c:forEach var="message" items="${messages}">
                    <c:choose>
                        <c:when test='${message.fromAccount.id == sessionScope.get("id")}'>
                            <div class="well msg right-msg">
                                <p>${message.msgText}</p>
                                <small class="form-text text-muted">
                                        ${message.sendingDate.toLocaleString()}
                                </small>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="well msg left-msg">
                                <p>${message.msgText}</p>
                                <small class="form-text text-muted">
                                        ${message.sendingDate.toLocaleString()}
                                </small>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </div>
            
            <div class="form-group">
                <textarea id="textMessage" class="form-control" name="message" rows="4"></textarea>
            </div>
            <a id="sendMsg" class="btn btn-primary" disabled="true">Send message</a>
        </div>
    </div>

</div>

<%@include file="common/footer.jsp" %>
