<%@include file="common/header.jsp" %>

<jsp:setProperty name="MenuBean" property="friends" value="active"/>
<%@include file="common/MenuPanel.jsp" %>

<div class="container">
    <div class="row">
        <h1>
            Search results
        </h1>
    </div>
    <div class="row" id="account-search-results">
        <c:forEach var="entity" items="${accounts}">
            <%@include file="common/searchEntityCard.jsp" %>
        </c:forEach>
    </div>
    <div class="row">
        <ul class="pagination">
            <c:forEach var="index" begin='1' end='${accountPages}'>
                <li class="page" type="account"><a type="button" class="btn btn-default" name="${index}">${index}</a>
                </li>
            </c:forEach>
        </ul>
    </div>

    <div class="row" id="group-search-results">
        <c:forEach var="entity" items="${groups}">
            <%@include file="common/searchEntityCard.jsp" %>
        </c:forEach>
    </div>
    <div class="row">
        <ul class="pagination">
            <c:forEach var="index" begin='1' end='${groupPages}'>
                <li class="page" type="group"><a type="button" class="btn btn-default" name="${index}">${index}</a>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
<div id="pattern" hidden>${pattern}</div>

<%@include file="common/footer.jsp" %>
