<%@include file="common/header.jsp" %>

<jsp:setProperty name="MenuBean" property="home" value="active"/>
<%@include file="common/MenuPanel.jsp" %>
<div class="container">
    <div class="row profile">
        <div class="col-md-6">
            <div class="page-content">
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-user">
                    <h1>
                        <div class="profile-user-name">
                            <c:out value='${group.groupName}'/>
                        </div>
                    </h1>
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td><strong>Registration date:</strong></td>
                            <td><c:out value='${registrationDate}'/></td>
                        </tr>

                        <tr>
                            <td><strong>Creator:</strong></td>
                            <td><a href="${pageContext.request.contextPath}/account/${group.creator.id}/home">
                                <c:out value='${group.creator.name}'/> <c:out value='${group.creator.familyName}'/>
                            </a>
                            </td>
                        </tr>
                        <c:choose>
                            <c:when test='${group.addInfo != null}'>
                                <tr>
                                    <td><strong>Additional information:</strong></td>
                                    <td><c:out value='${group.addInfo}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        </tbody>
                    </table>
                </div>
            </div>

            <%--POSTS START--%>
            <p>Post on wall:</p>
            <form role="form" method="post" action="${pageContext.request.contextPath}/group/${group.id}/sendPost">
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="4"></textarea>
                </div>
                <input id="toGroupIdMessage" name="toGroupId" value="${group.id}" hidden/>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <div class="page-content" id="messages-list">
                <c:forEach var="post" items="${posts}">
                    <%@include file="common/post.jsp" %>
                </c:forEach>
            </div>
            <%--POSTS END--%>
        </div>

        <div class="col-md-4">
            <div class="page-sidebar">
                <!-- SIDEBAR GROUPPIC -->
                <div class="profile-userpic img-responsive">
                        <c:choose>
                            <c:when test="${group.picture != null}">
                                <cl:image src="${group.id}_group" format="jpg" invalidate="true" width="250" height="250"/>
                            </c:when>
                            <c:otherwise>
                                <img src="${pageContext.request.contextPath}/img/group-default.jpg" class="img-responsive">
                            </c:otherwise>
                        </c:choose>
                </div>
                <!-- END SIDEBAR GROUPPIC -->

                <!-- SIDEBAR BUTTONS -->
                <div class="sidebar-buttons">
                    <c:choose>
                        <c:when test='${sessionScope.get("id") == group.creator.id}'>
                            <a type="button" class="btn btn-success btn-sm"
                               href="${pageContext.request.contextPath}/groupSettings?groupId=${group.id}">Group settings</a>
                        </c:when>
                        <c:when test='${member != null}'>
                            <a type="button" class="btn btn-success btn-sm disabled">You are member</a>
                        </c:when>
                        <c:otherwise>
                            <a type="button" class="btn btn-success btn-sm"
                                href="${pageContext.request.contextPath}/joinGroup?groupId=${group.id}">Join group</a>
                        </c:otherwise>
                    </c:choose>
                    <a type="button" class="btn btn-success btn-sm"
                       href="${pageContext.request.contextPath}/group/${group.id}/members">Group members</a>
                </div>

                <!-- END SIDEBAR BUTTONS -->
            </div>
        </div>
    </div>
</div>
<br>
<br>

<%@include file="common/footer.jsp" %>