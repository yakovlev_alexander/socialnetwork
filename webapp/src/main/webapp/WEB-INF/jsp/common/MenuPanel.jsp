<nav class="navbar navbar-fixed-top navbar-inverse" id="menu-panel">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href='${pageContext.request.contextPath}/account/${sessionScope.get("id")}/home'>MySocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class=${MenuBean.home}>
                    <a href='${pageContext.request.contextPath}/account/${sessionScope.get("id")}/home'>Home</a></li>
                <li class=${MenuBean.friends}>
                    <a href="${pageContext.request.contextPath}/account/${sessionScope.get("id")}/friends">Friends</a>
                </li>
                <li class=${MenuBean.groups}>
                    <a href="${pageContext.request.contextPath}/account/${sessionScope.get("id")}/groups">Groups</a>
                </li>
                <li class=${MenuBean.messages}>
                    <a href="${pageContext.request.contextPath}/messages">My Messages</a>
                </li>
                <li class=${MenuBean.settings}>
                    <a href="${pageContext.request.contextPath}/settings">Settings</a>
                </li>
            </ul>

            <form class="navbar-form navbar-left" role="search" method="get" id="search-form"
                  action="${pageContext.request.contextPath}/show_search">
                <div class="form-group">
                    <input id="search-id" type="search" class="form-control" name="search-by-pattern"
                           placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <form class="navbar-form navbar-right">
                <strong>
                    <c:out value='${sessionScope.get("account").getName()}'/>
                    <c:out value='${sessionScope.get("account").getFamilyName()}'/>
                </strong>
                signed in (<a href="<c:url value='/logout'/>">sign out</a>)
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
