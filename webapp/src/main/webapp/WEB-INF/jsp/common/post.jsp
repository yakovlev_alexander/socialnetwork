<div class="panel panel-default">
    <div class="panel-heading">
        <p class="pull-right">
            <a href="${pageContext.request.contextPath}/${account != null ? "account/".concat(account.id) : "group/".concat(group.id)}/deletePost?id=${post.id}">
                Delete post
            </a>
        </p>
        <strong>
            Post from
            <a href="${pageContext.request.contextPath}/account/${post.fromAccount.id}/home">
                ${post.fromAccount.name} ${post.fromAccount.familyName}
            </a>
        </strong>
    </div>
    <div class="panel-body">
        <p>${post.msgText}</p>
    </div>
    <div class="panel-footer">
        <%--<p class="pull-right">--%>
        <small class="form-text text-muted">
            ${post.sendingDate.toLocaleString()}
        </small>
        <%--</p>--%>
    </div>
</div>
