        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/js/sockjs.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/settings-script.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/search-script.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/messages-script.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.cloudinary.js" type="text/javascript"></script>
    </body>
</html>

