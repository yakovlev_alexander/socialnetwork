<div class="col-sm-3">
    <div class="card">
        <canvas class="header-bg" width="250" height="70"></canvas>
        <div class="avatar">
            <c:choose>
                <c:when test="${entity.hasPic}">
                    <cl:image src="${entity.id}_${entity.type.toString()}" format="jpg" invalidate="true" width="150" height="150"/>
                </c:when>
                <c:otherwise>
                    <img src="${pageContext.request.contextPath}/img/${entity.type.toString()}-default.jpg">
                </c:otherwise>
            </c:choose>
        </div>
        <div class="content">
            <p>${entity.getPrimaryName()}
                <br>
                ${entity.getSecondaryName()}
            </p>
            <p>
                <a type="button" class="btn btn-default"
                   href="${pageContext.request.contextPath}/${entity.type.toString()}/${entity.id}/home">Show ${entity.type.toString()}</a>
            </p>
        </div>
    </div>
</div>