
<div class="col-sm-3">
    <div class="card">
        <canvas class="header-bg" width="250" height="70"></canvas>
        <div class="avatar">
            <c:choose>
                <c:when test="${account.picture != null}">
                    <cl:image src="${account.id}_account" format="jpg" invalidate="true" width="150" height="150"/>
                </c:when>
                <c:otherwise>
                    <img src="${pageContext.request.contextPath}/img/account-default.jpg">
                </c:otherwise>
            </c:choose>
            <%--<img src="${pageContext.request.contextPath}/getPicture?id=${account.id}&type=ACCOUNT"--%>
                 <%--alt=""/>--%>
        </div>
        <div class="content">
            <p>${account.getName()} ${account.getFamilyName()}
                <br>
                ${account.getEmail()}
            </p>
            <p>
                <a type="button" class="btn btn-default"
                   href="${pageContext.request.contextPath}/account/${account.id}/home">Show profile</a>
            </p>
        </div>
    </div>
</div>
