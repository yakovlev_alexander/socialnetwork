<div class="col-sm-3">
    <div class="card">
        <canvas class="header-bg" width="250" height="70"></canvas>
        <div class="avatar">
            <c:choose>
                <c:when test="${group.picture != null}">
                    <cl:image src="${group.id}_group" format="jpg" invalidate="true" width="150" height="150"/>
                </c:when>
                <c:otherwise>
                    <img src="${pageContext.request.contextPath}/img/group-default.jpg">
                </c:otherwise>
            </c:choose>
        </div>
        <div class="content">
            <p>${group.getGroupName()}
                <br>
                by ${group.getCreator().getName()} ${group.getCreator().getFamilyName()}
            </p>
            <p>
                <a type="button" class="btn btn-default"
                   href="${pageContext.request.contextPath}/group/${group.id}/home">Show group</a>
            </p>
        </div>
    </div>
</div>