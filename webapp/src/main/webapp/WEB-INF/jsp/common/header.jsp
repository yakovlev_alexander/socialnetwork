<%@page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cl" uri="http://cloudinary.com/jsp/taglib" %>
<jsp:useBean id="MenuBean" class="com.getjavajob.training.web1608.yakovleva.ui.utils.MenuBean" scope="application"/>
<html>
<head>
    <title>My Social</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css" type="text/css"/>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-style.css" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/home-page-style.css" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/register-page-style.css" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/friends-page-style.css" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/messages-style.css" type="text/css"/>
</head>
<body>