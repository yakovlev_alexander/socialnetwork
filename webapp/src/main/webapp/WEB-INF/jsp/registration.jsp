<%@include file="common/header.jsp" %>
<form action="do-register" method="post" role="form" class="form-horizontal" >
    <%--<input type='hidden' name='csrfmiddlewaretoken' valueId='brGfMU16YyyG2QEcpLqhb3Zh8AvkYkJt'/>--%>
    <div class="form-group required">
        <label class="col-md-2 control-label">Family name</label>
        <div class="col-md-4">
            <input class="form-control" id="id_family_name" maxlength="30" name="familyName"
                   placeholder="Family name" required="required" type="text"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">First name</label>
        <div class="col-md-4">
            <input class="form-control" id="id_first_name" maxlength="30" name="name"
                   placeholder="First name" required="required" type="text"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Middle name</label>
        <div class="col-md-4">
            <input class="form-control" id="id_middle_name" maxlength="30" name="middleName"
                   placeholder="Middle name" type="text"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">E-mail</label>
        <div class="col-md-4">
            <input class="form-control" id="id_email" name="email" placeholder="email"
                   required="required" type="email"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">Password</label>
        <div class="col-md-4">
            <input class="form-control" id="id_password1" name="pass" placeholder="Password"
                   required="required" type="password"/>
        </div>
    </div>
    <div class="form-group required">
        <label class="col-md-2 control-label">Password (again)</label>
        <div class="col-md-4">
            <input class="form-control" id="id_password2" name="password2"
                   placeholder="Password (again)" required="required" type="password"/>
        </div>
    </div>

    <div class="form-group required">
        <label class="col-md-2 control-label"></label>
        <div class="col-md-4">
            <div class="checkbox">
                <label>
                    <input required="required" type="checkbox"/>I have read and agree to the Terms of Service
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-star"></span> Register
            </button>
        </div>
    </div>
    <c:choose>
        <c:when test='${sessionScope.get("reason") != null}'>
            <div class="alert alert-danger">
                <c:out value='${sessionScope.get("reason")}'/>
            </div>
        </c:when>
    </c:choose>
</form>
<%@include file="common/footer.jsp" %>