<%@include file="common/header.jsp" %>

<jsp:setProperty name="MenuBean" property="groups" value="active"/>
<%@include file="common/MenuPanel.jsp" %>

<div class="container">
    <div class="row">
        <h1>
            ${account.name} ${account.familyName} groups
        </h1>
    </div>
    <div class="row">
        <c:forEach var="group" items="${groups}">

            <%@include file="common/groupCard.jsp"%>

        </c:forEach>
    </div>
</div>

<%@include file="common/footer.jsp" %>
