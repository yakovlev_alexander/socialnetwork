<%@include file="common/header.jsp" %>

<jsp:setProperty name="MenuBean" property="home" value="active"/>
<%@include file="common/MenuPanel.jsp" %>
<div class="container">
    <div class="row profile">
        <div class="col-md-4">
            <div class="page-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <c:choose>
                        <c:when test="${account.picture != null}">
                            <cl:image src="${account.id}_account" format="jpg" width="250" height="250" invalidate="true" />
                        </c:when>
                        <c:otherwise>
                            <img src="${pageContext.request.contextPath}/img/account-default.jpg" class="img-responsive">
                        </c:otherwise>
                    </c:choose>
                </div>
                <!-- END SIDEBAR USERPIC -->

                <!-- SIDEBAR BUTTONS -->
                <div class="sidebar-buttons">
                    <c:if test='${sessionScope.get("id") == account.id}'>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/groupSettings">Create group</a>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/makeXML?id=${account.id}">Export to XML</a>
                    </c:if>
                    <c:if test='${relation == "STRANGER"}'>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/sendFriendRequest?toId=${account.id}">Add to
                            friends</a>
                    </c:if>
                    <c:if test='${relation == "FOLLOWER"}'>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/acceptFriendRequest?fromId=${account.id}">Accept
                            friendship</a>
                    </c:if>
                    <c:if test='${relation == "FOLLOWED"}'>
                        <a type="button" class="btn btn-primary btn-sm disabled">Request sent</a>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/deleteRequest?accId=${account.id}">Withdraw
                            request</a>
                    </c:if>
                    <c:if test='${relation == "FRIEND"}'>
                        <a type="button" class="btn btn-primary btn-sm disabled">Your friend</a>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/deleteRequest?accId=${account.id}">Remove from
                            friends</a>
                    </c:if>

                    <c:if test='${sessionScope.get("id") != account.id}'>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/messages?recipientId=${account.id}">Send message</a>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/account/${account.id}/friends">Show friends</a>
                        <a type="button" class="btn btn-primary btn-sm"
                           href="${pageContext.request.contextPath}/account/${account.id}/groups">Show groups</a>
                    </c:if>
                </div>

                <!-- END SIDEBAR BUTTONS -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="page-content">
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-user">
                    <h1>
                        <div class="profile-user-name">
                            <c:out value='${account.name}'/>
                            <c:choose>
                                <c:when test='${account.middleName != null}'>
                                    <c:out value='${account.middleName}'/>
                                </c:when>
                            </c:choose>
                            <c:out value='${account.familyName}'/>
                        </div>
                    </h1>
                    <%--<div class="profile-user-contact">--%>
                    <table class="table table-striped">
                        <tbody>
                        <c:choose>
                            <c:when test='${registrationDate != null}'>
                                <tr>
                                    <td><strong>Registration date:</strong></td>
                                    <td><c:out value='${registrationDate}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${birthday != null}'>
                                <tr>
                                    <td><strong>Birthday:</strong></td>
                                    <td><c:out value='${birthday}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.email != null}'>
                                <tr>
                                    <td><strong>Email:</strong></td>
                                    <td><c:out value='${account.email}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.personalPhone != null}'>
                                <tr>
                                    <td><strong>Personal phone:</strong></td>
                                    <td><c:out value='${account.personalPhone}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.workPhone != null}'>
                                <tr>
                                    <td><strong>Work phone:</strong></td>
                                    <td><c:out value='${account.workPhone}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.icq != null}'>
                                <tr>
                                    <td><strong>ICQ:</strong></td>
                                    <td><c:out value='${account.icq}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.skype != null}'>
                                <tr>
                                    <td><strong>Skype:</strong></td>
                                    <td><c:out value='${account.skype}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.addInfo != null}'>
                                <tr>
                                    <td><strong>Additional information:</strong></td>
                                    <td><c:out value='${account.addInfo}'/></td>
                                </tr>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test='${account.skills != null}'>
                                <tr>
                                    <td><strong>Skills:</strong></td>
                                    <td>
                                        <c:forEach var='skill' items='${account.skills}'>
                                            <c:out value="${skill}"/><br>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:when>
                        </c:choose>
                        </tbody>
                    </table>
                    <%--</div>--%>
                </div>
                <!-- END SIDEBAR USER TITLE -->
            </div>
            <%--POSTS START--%>
            <p>Post on wall:</p>
            <form role="form" method="post" action="${pageContext.request.contextPath}/account/${account.id}/sendPost">
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="4"></textarea>
                </div>
                <input id="toAccIdMessage" name="toAccountId" value="${account.id}" hidden/>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <div class="page-content" id="messages-list">
                <c:forEach var="post" items="${posts}">
                    <%@include file="common/post.jsp" %>
                </c:forEach>
            </div>
            <%--POSTS END--%>
        </div>
    </div>
</div>
<br>
<br>

<%@include file="common/footer.jsp" %>