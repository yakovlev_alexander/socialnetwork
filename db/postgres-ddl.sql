CREATE SCHEMA IF NOT EXISTS FINAL_PROJECT_DB;
CREATE TABLE IF NOT EXISTS final_project_db.ACCOUNT_TBL (
    ID SERIAL PRIMARY KEY,
    NAME VARCHAR(255) NOT NULL,
    FAMILY_NAME VARCHAR(255) NOT NULL,
    MIDDLE_NAME VARCHAR(255),
    BIRTH_DAY DATE,
    PERSONAL_PHONE VARCHAR(20),
    WORK_PHONE VARCHAR(20),
    EMAIL VARCHAR(30) NOT NULL UNIQUE,
    PASS VARCHAR(100),
    ICQ VARCHAR(10),
    SKYPE VARCHAR(30),
    ADD_INFO TEXT,
    REGISTRATION_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ACTIVE BOOLEAN NOT NULL DEFAULT TRUE,
    PIC BYTEA
);
CREATE TABLE IF NOT EXISTS final_project_db.FRIENDS_TBL (
    ACCOUNT_ID INTEGER NOT NULL,
    FRIEND_ID INTEGER NOT NULL,
    RESPONSE BOOLEAN NOT NULL DEFAULT FALSE,
    CONSTRAINT FRIENDS_PK PRIMARY KEY (ACCOUNT_ID, FRIEND_ID),
    CONSTRAINT ACCOUNT_ID_FK FOREIGN KEY (ACCOUNT_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID),
    CONSTRAINT FRIEND_ID_FK FOREIGN KEY (FRIEND_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID)
);
CREATE TABLE IF NOT EXISTS final_project_db.GROUP_TBL (
    ID SERIAL PRIMARY KEY,
    GROUP_NAME VARCHAR(255) NOT NULL UNIQUE,
    OWNER INTEGER NOT NULL,
    ADD_INFO TEXT,
    REGISTRATION_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ACTIVE BOOLEAN NOT NULL DEFAULT TRUE,
    PIC BYTEA
);
CREATE TABLE IF NOT EXISTS final_project_db.GROUP_MEMBERS_TBL (
    GROUP_ID INTEGER NOT NULL,
    MEMBER_ID INTEGER NOT NULL,
    OWNER BOOLEAN NOT NULL DEFAULT FALSE,
    ADMIN BOOLEAN NOT NULL DEFAULT FALSE,
    CONSTRAINT GROUP_MEMBER_PK PRIMARY KEY (GROUP_ID, MEMBER_ID),
    CONSTRAINT GROUP_ID_FK FOREIGN KEY (GROUP_ID) REFERENCES final_project_db.GROUP_TBL(ID),
    CONSTRAINT MEMBER_ID_FK FOREIGN KEY (MEMBER_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID)
);
CREATE TABLE IF NOT EXISTS final_project_db.SKILLS_TBL (
    ACCOUNT_ID INTEGER NOT NULL,
    SKILL VARCHAR(255) NOT NULL,
    CONSTRAINT ACCOUNT_SKILL_PK PRIMARY KEY (ACCOUNT_ID, SKILL),
    CONSTRAINT ACCOUNT_SKILL_ID_FK FOREIGN KEY (ACCOUNT_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID)
);
CREATE TABLE IF NOT EXISTS final_project_db.MSG_TBL (
    ID SERIAL PRIMARY KEY,
    SENDER_ID INTEGER NOT NULL,
    RECIPIENT_ID INTEGER NOT NULL,
    MSG_TEXT TEXT NOT NULL,
    SENDING_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT SENDER_ID_FK FOREIGN KEY (SENDER_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID),
    CONSTRAINT RECIPIENT_ID_FK FOREIGN KEY (RECIPIENT_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID)
);
CREATE TABLE IF NOT EXISTS final_project_db.ACCOUNT_POST_TBL (
    ID SERIAL PRIMARY KEY,
    SENDER_ID INTEGER NOT NULL,
    RECIPIENT_ID INTEGER NOT NULL,
    MSG_TEXT TEXT NOT NULL,
    SENDING_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT ACCOUNT_POST_SENDER_ID_FK FOREIGN KEY (SENDER_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID),
    CONSTRAINT ACCOUNT_POST_RECIPIENT_ID_FK FOREIGN KEY (RECIPIENT_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID)
);
CREATE TABLE IF NOT EXISTS final_project_db.GROUP_POST_TBL (
    ID SERIAL PRIMARY KEY,
    SENDER_ID INTEGER NOT NULL,
    RECIPIENT_ID INTEGER NOT NULL,
    MSG_TEXT TEXT NOT NULL,
    SENDING_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT GROUP_POST_SENDER_ID_FK FOREIGN KEY (SENDER_ID) REFERENCES final_project_db.ACCOUNT_TBL(ID),
    CONSTRAINT GROUP_POST_RECIPIENT_ID_FK FOREIGN KEY (RECIPIENT_ID) REFERENCES final_project_db.GROUP_TBL(ID)
);
