package com.getjavajob.training.web1608.yakovleva.service;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.common.post.GroupPost;
import com.getjavajob.training.web1608.yakovleva.dao.AccountDao;
import com.getjavajob.training.web1608.yakovleva.dao.GroupDao;
import com.getjavajob.training.web1608.yakovleva.dao.post.GroupPostDao;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GroupServiceTest extends ServiceTest {
    private GroupService groupService;
    private GroupDao groupDao;
    private AccountDao accountDao;
    private GroupPostDao postDao;

    private Group testGroup;
    private Account testAccount;

    @Before
    public void setUp() throws Exception {
        groupDao = mock(GroupDao.class);
        accountDao = mock(AccountDao.class);
        postDao = mock(GroupPostDao.class);
        groupService = new GroupService(groupDao, accountDao, postDao);
        testGroup = new Group(1);
        testAccount = new Account(1, "Petya", "Titkin", "titkin@mail.ru", "qwerty");
        testGroup.setCreator(testAccount);
        testGroup.setGroupName("Sample name for a group");
    }

    @Test
    public void createGroupTest() throws Exception {
        when(groupDao.create(testGroup)).thenReturn(testGroup);
        assertEquals(groupService.createEntity(testGroup), testGroup);
    }

    @Test
    public void getAllTest() throws Exception {
        when(groupDao.getAll()).thenReturn(getTestList(testGroup));
        assertEquals(groupService.getAllEntities(), getTestList(testGroup));
    }

    @Test
    public void updateAccountTest() throws Exception {
        groupService.updateEntity(testGroup);
        verify(groupDao).update(testGroup);
    }

    @Test
    public void deleteAccountTest() throws Exception {
        groupService.deleteEntity(testGroup);
        verify(groupDao).update(testGroup);
    }

    @Test
    public void getGroupCreatorTest() throws Exception {
        when(groupDao.getEntityById(testGroup.getId())).thenReturn(testGroup);
        assertEquals(groupService.getGroupCreator(testGroup), testGroup.getCreator());
    }

    @Test
    public void getGroupMembersTest() throws Exception {
        testGroup.setMembers(getTestList(testAccount));
        when(groupDao.getEntityById(testGroup.getId())).thenReturn(testGroup);
        assertEquals(groupService.getGroupMembers(testGroup), testGroup.getMembers());
    }

    @Test
    public void deletePostTest() throws Exception {
        groupService.deletePost(1);
        verify(postDao).deletePostById(1);
    }

    @Test
    public void getEntityPostsTest() throws Exception {
        when(groupService.getEntityById(testGroup.getId())).thenReturn(testGroup);
        groupService.getEntityPosts(testGroup);
        verify(postDao).getAllEntityPosts(testGroup);
    }

    @Test
    public void sendPostTest() throws Exception {
        when(accountDao.getEntityById(testAccount.getId())).thenReturn(testAccount);
        when(groupService.getEntityById(testGroup.getId())).thenReturn(testGroup);
        groupService.sendPost(testAccount, testGroup, "post");
        verify(postDao).createPost(new GroupPost(testAccount, testGroup, "post"));
    }
}
