package com.getjavajob.training.web1608.yakovleva.service;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.common.post.AccountPost;
import com.getjavajob.training.web1608.yakovleva.dao.AccountDao;
import com.getjavajob.training.web1608.yakovleva.dao.FriendRequestDao;
import com.getjavajob.training.web1608.yakovleva.dao.MsgDao;
import com.getjavajob.training.web1608.yakovleva.dao.post.AccountPostDao;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountServiceTest extends ServiceTest {
    private AccountService accountService;
    private AccountDao accountDao;
    private FriendRequestDao requestDao;
    private MsgDao msgDao;
    private AccountPostDao postDao;

    private Account from;
    private Account to;

    private void mockAccountInvocation(Account account) {
        when(accountDao.getEntityById(account.getId())).thenReturn(account);
    }

    @Before
    public void setUp() throws Exception {
        accountDao = mock(AccountDao.class);
        requestDao = mock(FriendRequestDao.class);
        msgDao = mock(MsgDao.class);
        postDao = mock(AccountPostDao.class);
        accountService = new AccountService(accountDao, requestDao, msgDao, postDao);
        from = new Account(1, "Petya", "Titkin", "titkin@mail.ru", "qwerty");
        to = new Account(2, "Vasya", "Titkin", "vtitkin@mail.ru", "qwerty");
    }

    @Test
    public void createAccountTest() throws Exception {
        when(accountDao.create(from)).thenReturn(from);
        assertEquals(accountService.createEntity(from), from);
    }

    @Test
    public void getAllTest() throws Exception {
        when(accountDao.getAll()).thenReturn(getTestList(to));
        assertEquals(accountService.getAllEntities(), getTestList(to));
    }

    @Test
    public void updateAccountTest() throws Exception {
        accountService.updateEntity(from);
        verify(accountDao).update(from);
    }

    @Test
    public void deleteAccountTest() throws Exception {
        accountService.deleteEntity(from);
        verify(accountDao).update(from);
    }

    @Test
    public void getGroupsOfAccountTest() throws Exception {
        List<Group> groups = getTestList(new Group(1));
        Account account = mock(Account.class);
        when(account.getAccountGroups()).thenReturn(groups);
        mockAccountInvocation(account);
        assertEquals(accountService.getGroupsOfAccount(account), groups);
    }

    @Test
    public void getSkillsTest() throws Exception {
        List<String> skills = new LinkedList<>();
        skills.add("Java");
        skills.add("SQL");
        from.setSkills(skills);
        when(accountService.getEntityById(from.getId())).thenReturn(from);
        assertEquals(accountService.getSkills(from), skills);
    }

    @Test
    public void sendFriendRequestTest() throws Exception {
        Account fakeFrom = mock(Account.class);
        Account fakeTo = mock(Account.class);
        when(accountDao.getEntityById(from.getId())).thenReturn(fakeFrom);
        when(accountDao.getEntityById(to.getId())).thenReturn(fakeTo);
        accountService.sendFriendRequest(from, to);
        verify(fakeFrom).addRequestsFromMe(fakeTo);
    }

    @Test
    public void acceptFriendRequestTest() throws Exception {
        mockAccountInvocation(from);
        mockAccountInvocation(to);
        accountService.acceptFriendRequest(from, to);
        verify(requestDao).acceptRequest(from, to);
    }

    @Test
    public void removeFriendTest() throws Exception {
        mockAccountInvocation(from);
        mockAccountInvocation(to);
        accountService.removeFriend(from, to);
        verify(requestDao).removeRequest(to, from);
    }

    @Test
    public void rejectFriendTest() throws Exception {
        mockAccountInvocation(from);
        mockAccountInvocation(to);
        accountService.rejectFriendRequest(from, to);
        verify(requestDao).rejectRequest(from, to);
    }

    @Test
    public void getFriendsListTest() throws Exception {
        List<Account> friends = getTestList(to);
        Account account = mock(Account.class);
        when(account.getFriends()).thenReturn(friends);
        mockAccountInvocation(account);
        assertEquals(accountService.getFriendsList(account), friends);
    }

    @Test
    public void getFollowersListTest() throws Exception {
        List<Account> followers = getTestList(to);
        Account account = mock(Account.class);
        when(account.getRequestedToMe()).thenReturn(followers);
        mockAccountInvocation(account);
        assertEquals(accountService.getFollowersList(account), followers);
    }

    @Test
    public void getFollowedListTest() throws Exception {
        List<Account> followed = getTestList(to);
        Account account = mock(Account.class);
        when(account.getRequestedFromMe()).thenReturn(followed);
        mockAccountInvocation(account);
        assertEquals(accountService.getFollowedList(account), followed);
    }

    @Test
    public void deletePostTest() throws Exception {
        accountService.deletePost(1);
        verify(postDao).deletePostById(1);
    }

    @Test
    public void getEntityPostsTest() throws Exception {
        when(accountDao.getEntityById(from.getId())).thenReturn(from);
        accountService.getEntityPosts(from);
        verify(postDao).getAllEntityPosts(from);
    }

    @Test
    public void sendPostTest() throws Exception {
        when(accountDao.getEntityById(from.getId())).thenReturn(from);
        when(accountDao.getEntityById(to.getId())).thenReturn(to);
        accountService.sendPost(from, to, "post");
        verify(postDao).createPost(new AccountPost(from, to, "post"));
    }
}
