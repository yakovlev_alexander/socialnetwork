package com.getjavajob.training.web1608.yakovleva.service;

import java.util.LinkedList;
import java.util.List;

public abstract class ServiceTest {
    protected  <E> List<E> getTestList(E... entities) {
        List<E> list = new LinkedList<>();
        for (E entity : entities) {
            list.add(entity);
        }
        return list;
    }
}
