package com.getjavajob.training.web1608.yakovleva.service;

import com.getjavajob.training.web1608.yakovleva.common.*;
import com.getjavajob.training.web1608.yakovleva.common.PrivateMsg;
import com.getjavajob.training.web1608.yakovleva.common.post.AccountPost;
import com.getjavajob.training.web1608.yakovleva.dao.AccountDao;
import com.getjavajob.training.web1608.yakovleva.dao.FriendRequestDao;
import com.getjavajob.training.web1608.yakovleva.dao.MsgDao;
import com.getjavajob.training.web1608.yakovleva.dao.post.AccountPostDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class AccountService extends AbstractService<Account, AccountPost> {
    private FriendRequestDao requestDao;
    private MsgDao msgDao;

    @Autowired
    public AccountService(AccountDao accountDao, FriendRequestDao requestDao, MsgDao msgDao, AccountPostDao postDao) {
        super(accountDao, postDao);
        this.requestDao = requestDao;
        this.msgDao = msgDao;
    }

    @Override
    protected AccountDao getDao() {
        return (AccountDao) super.getDao();
    }

    @Transactional
    public Account getAccountByEmail(String email) {
        return getDao().getAccountByEmail(email);
    }

    @Transactional
    public List<Group> getGroupsOfAccount(Account account) {
        List<Group> groups = new LinkedList<>();
        groups.addAll(getEntityById(account.getId()).getAccountGroups());
        return groups;
    }

    @Transactional
    public void sendFriendRequest(Account from, Account to) {
        Account daoFromAccount = getEntityById(from.getId());
        Account daoToAccount = getEntityById(to.getId());
        daoFromAccount.addRequestsFromMe(daoToAccount);
    }

    @Transactional
    public void acceptFriendRequest(Account from, Account to) {
        Account daoFromAccount = getEntityById(from.getId());
        Account daoToAccount = getEntityById(to.getId());
        requestDao.acceptRequest(daoFromAccount, daoToAccount);
    }

    @Transactional
    public void rejectFriendRequest(Account from, Account to) {
        Account daoFromAccount = getEntityById(from.getId());
        Account daoToAccount = getEntityById(to.getId());
        requestDao.rejectRequest(daoFromAccount, daoToAccount);
    }

    @Transactional
    public void removeFriend(Account from, Account to) {
        Account daoFromAccount = getEntityById(from.getId());
        Account daoToAccount = getEntityById(to.getId());
        if (requestDao.getFriendRequest(daoFromAccount, daoToAccount) != null) {
            requestDao.removeRequest(daoFromAccount, daoToAccount);
        } else {
            requestDao.removeRequest(daoToAccount, daoFromAccount);
        }
    }

    @Transactional
    public List<Account> getFriendsList(Account account) {
        List<Account> accounts = new LinkedList<>();
        accounts.addAll(getEntityById(account.getId()).getFriends());
        return accounts;
    }

    @Transactional
    public List<Account> getFollowersList(Account account) {
        List<Account> accounts = new LinkedList<>();
        accounts.addAll(getEntityById(account.getId()).getRequestedToMe());
        return accounts;
    }

    @Transactional
    public List<Account> getFollowedList(Account account) {
        List<Account> accounts = new LinkedList<>();
        accounts.addAll(getEntityById(account.getId()).getRequestedFromMe());
        return accounts;
    }

    @Transactional
    public List<String> getSkills(Account account) {
        List<String> skills = new LinkedList<>();
        skills.addAll(getEntityById(account.getId()).getSkills());
        return skills;
    }

    @Transactional
    public List<PrivateMsg> getAccountMessages(Account account) {
        List<PrivateMsg> msgs = new LinkedList<>();
        msgs.addAll(msgDao.getAllAccountMsgs(account));
        return msgs;
    }

    @Transactional
    public void sendMessage(long fromId, long toId, String text) {
        Account daoFromAccount = getEntityById(fromId);
        Account daoToAccount = getEntityById(toId);
        daoFromAccount.addMsqSent(daoToAccount, text);
    }

    @Transactional
    public List<PrivateMsg> getMessagesOfAccounts(Account fist, Account second) {
        if (fist == null || second == null) {
            return null;
        }
        Account daoFirst = getDao().getEntityById(fist.getId());
        Account daoSecond = getDao().getEntityById(second.getId());
        return msgDao.getMsgsOfPair(daoFirst, daoSecond);
    }

    @Transactional
    public Relation getRelation(long fromId, long toId) {
        if (fromId == toId) {
            return Relation.ONE_ACCOUNT;
        }
        Account from = getDao().getEntityById(fromId);
        Account to = getDao().getEntityById(toId);
        FriendRequest requestReceived = requestDao.getFriendRequest(to, from);
        FriendRequest requestSent = requestDao.getFriendRequest(from, to);
        FriendRequest request = requestReceived != null ? requestReceived : requestSent;
        if (request == null) {
            return Relation.STRANGER;
        } else if (request.isAccepted()) {
            return Relation.FRIEND;
        } else if (request.getFromAccount().equals(from)) {
            return Relation.FOLLOWED;
        } else if (request.getToAccount().equals(from)) {
            return Relation.FOLLOWER;
        } else {
            return Relation.ONE_ACCOUNT;
        }
    }

    @Transactional
    @Override
    public AccountPost sendPost(Account from, Account to, String text) {
        Account daoFrom = getDao().getEntityById(from.getId());
        Account daoTo = getDao().getEntityById(to.getId());
        return getPostDao().createPost(new AccountPost(daoFrom, daoTo, text));
    }
}

