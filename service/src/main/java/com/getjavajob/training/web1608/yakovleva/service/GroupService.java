package com.getjavajob.training.web1608.yakovleva.service;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.common.post.GroupPost;
import com.getjavajob.training.web1608.yakovleva.dao.AccountDao;
import com.getjavajob.training.web1608.yakovleva.dao.GroupDao;
import com.getjavajob.training.web1608.yakovleva.dao.post.GroupPostDao;
import com.getjavajob.training.web1608.yakovleva.dao.utils.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class GroupService extends AbstractService<Group, GroupPost> {
    private AccountDao accountDao;

    @Autowired
    public GroupService(GroupDao groupDao, AccountDao accountDao, GroupPostDao postDao) {
        super(groupDao, postDao);
        this.accountDao = accountDao;
    }

    @Override
    protected GroupDao getDao() {
        return (GroupDao) super.getDao();
    }

    @Transactional
    @Override
    public GroupPost sendPost(Account from, Group to, String text) {
        Account daoFrom = accountDao.getEntityById(from.getId());
        Group daoTo = getDao().getEntityById(to.getId());
        return getPostDao().createPost(new GroupPost(daoFrom, daoTo, text));
    }

    @Transactional
    public Account getGroupCreator(Group group) {
        Group daoGroup = getEntityById(group.getId());
        return daoGroup.getCreator();
    }

    @Transactional
    public List<Account> getGroupMembers(Group group) {
        List<Account> accounts = new LinkedList<>();
        accounts.addAll(getEntityById(group.getId()).getMembers());
        return accounts;
    }

    @Transactional
    public Group getGroupByName(String name) {
        return getDao().getGroupByName(name);
    }

    @Transactional
    public void addMember(long groupId, long memberId) {
        getDao().getEntityById(groupId).addMember(accountDao.getEntityById(memberId));
    }

    @Transactional
    @Override
    public Group createEntity(Group group) throws ServiceException {
        Account creator = group.getCreator();
        if(creator != null) {
            Account creatorDao = accountDao.getEntityById(creator.getId());
            group.setCreator(creatorDao);
            Group newGroup = super.createEntity(group);
            newGroup.addMember(creatorDao);
            return newGroup;
        } else {
            throw new ServiceException("Creator wasn't set");
        }
    }
}
