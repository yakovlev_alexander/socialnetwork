package com.getjavajob.training.web1608.yakovleva.service;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.SNEntity;
import com.getjavajob.training.web1608.yakovleva.common.post.EntityPost;
import com.getjavajob.training.web1608.yakovleva.dao.AbstractDao;
import com.getjavajob.training.web1608.yakovleva.dao.post.EntityPostDao;
import com.getjavajob.training.web1608.yakovleva.dao.utils.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public abstract class AbstractService<E extends SNEntity, T extends EntityPost<E>> {
    private AbstractDao<E> dao;
    private EntityPostDao<E, T> postDao;

    @Autowired
    public AbstractService(AbstractDao<E> dao, EntityPostDao<E, T> postDao) {
        this.dao = dao;
        this.postDao = postDao;
    }

    public abstract T sendPost(Account from, E to, String text);

    protected AbstractDao<E> getDao() {
        return dao;
    }

    protected EntityPostDao<E, T> getPostDao() {
        return postDao;
    }

    @Transactional
    public E createEntity(E entity) throws ServiceException {
        try {
            return getDao().create(entity);
        } catch (DaoException e) {
            throw new ServiceException("Can't create entity", e);
        }
    }

    @Transactional
    public List<E> getAllEntities() {
        return getDao().getAll();
    }

    @Transactional
    public E getEntityById(long id) {
        return getDao().getEntityById(id);
    }

    @Transactional
    public List<E> getEntitiesByPattern(String pattern, int startEntity, int count) {
        return getDao().getEntitiesByPattern(pattern, startEntity, count);
    }

    @Transactional
    public long getSearchEntitiesCount(String pattern) {
        return getDao().getSearchEntitiesCount(pattern);
    }

    @Transactional
    public void updateEntity(E entity) {
        getDao().update(entity);
    }

    @Transactional
    public void deleteEntity(E entity) {
        entity.setActive(false);
        updateEntity(entity);
    }

    @Transactional
    public List<T> getEntityPosts(E entity) {
        E daoEntity = getDao().getEntityById(entity.getId());
        return postDao.getAllEntityPosts(daoEntity);
    }

    @Transactional
    public void deletePost(long postId) {
        postDao.deletePostById(postId);
    }
}
