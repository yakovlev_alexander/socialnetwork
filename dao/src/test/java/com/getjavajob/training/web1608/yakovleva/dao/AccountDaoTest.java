package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:social-dao-context.xml", "classpath:social-dao-context-test.xml"})
public class AccountDaoTest extends DaoTest {
    @Autowired
    private AccountDao dao;

    private List<Account> accounts = createAccounts();

    @Transactional
    @Test
    public void getEntityByIdTest() throws Exception {
        Account account = dao.getEntityById(1);
        assertEquals(account, accounts.get(0));
        account = dao.getEntityById(2);
        assertEquals(account, accounts.get(1));
        account = dao.getEntityById(3);
        assertEquals(account, accounts.get(2));
    }

    @Transactional
    @Test
    public void getEntityByNonExistentIdTest() throws Exception {
        assertNull(dao.getEntityById(4));
    }

    @Transactional
    @Test
    public void getAllTest() throws Exception {
        List<Account> daoAccounts = dao.getAll();
        assertEquals(daoAccounts.get(0), accounts.get(0));
        assertEquals(daoAccounts.get(1), accounts.get(1));
        assertEquals(daoAccounts.get(2), accounts.get(2));
    }

    @Transactional
    @Test
    public void updateTest() throws Exception {
        Account account = dao.getEntityById(3);
        account.setEmail("new_email@gmail.com");
//         dao.getEntityById(3).setEmail("new_email@gmail.com");
        dao.update(account);
        assertEquals(dao.getEntityById(3).getEmail(), "new_email@gmail.com");
    }

    @Transactional
    @Test
    public void createTest() throws Exception {
        Account account = new Account("Petya", "Titkin", "titkin@mail.ru", "qwerty");
        accounts.add(dao.create(account));
        assertEquals(dao.getEntityById(4), accounts.get(3));
    }

    @Transactional
    @Test
    public void getAccountByEmailTest() throws Exception {
        assertEquals(dao.getAccountByEmail("mamedov@gmail.com"), accounts.get(1));
    }

    @Transactional
    @Test
    public void getAccountByNonExistentEmailTest() throws Exception {
        assertNull(dao.getAccountByEmail("mamedovE@gmail.com"));
    }

    @Transactional
    @Test
    public void friendsConnectionsTest() throws Exception {
        assertTrue(dao.getEntityById(1).getFriends().contains(accounts.get(2)));
    }

    @Transactional
    @Test
    public void followersConnectionsTest() throws Exception {
        assertTrue(dao.getEntityById(1).getRequestedToMe().contains(accounts.get(1)));
    }

    @Transactional
    @Test
    public void followedConnectionsTest() throws Exception {
        System.out.println(Arrays.toString(dao.getEntityById(2).getRequestedFromMe().toArray()));
        assertTrue(dao.getEntityById(2).getRequestedFromMe().contains(accounts.get(0)));
    }

    @Transactional
    @Test
    public void getSkillsTest() throws Exception {
        List<String> list = dao.getEntityById(1).getSkills();
        assertTrue(list.contains("JavaScript"));
        assertTrue(list.contains("jQuery"));
        assertTrue(list.contains("Spring Framework"));
        assertTrue(list.contains("Java Core"));
    }

    @Transactional
    @Test
    public void getGroupsOfAccountTest() throws Exception {
        List<Group> testGroups = dao.getEntityById(3).getAccountGroups();
        assertTrue(testGroups.contains(createGroups().get(0)));
        assertTrue(testGroups.contains(createGroups().get(1)));
        assertFalse(testGroups.contains(createGroups().get(2)));
    }

    @Test
    public void getEntityByPatternTest_a() throws Exception {
        List<Account> accounts = dao.getEntitiesByPattern("a",0,1);
        assertEquals(accounts.get(0), createAccounts().get(0));
        assertEquals(accounts.size(), 1);
        accounts = dao.getEntitiesByPattern("a",1,1);
        assertEquals(accounts.get(0), createAccounts().get(1));
        System.out.println(accounts);
    }

    @Test
    public void getEntityByPatternTest_vi() throws Exception {
        List<Account> accounts = dao.getEntitiesByPattern("vI",0,1);
        assertEquals(accounts.get(0), createAccounts().get(2));
        assertEquals(accounts.size(), 1);
    }

    @Test
    public void getSearchQuerySizeTest() throws Exception {
        assertEquals(dao.getSearchEntitiesCount("vi"), 1);
        assertEquals(dao.getSearchEntitiesCount("a"), 3);
        assertEquals(dao.getSearchEntitiesCount("ov"), 2);
    }
}
