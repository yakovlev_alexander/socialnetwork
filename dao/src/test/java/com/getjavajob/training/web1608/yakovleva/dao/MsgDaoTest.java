package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.PrivateMsg;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:social-dao-context.xml", "classpath:social-dao-context-test.xml"})
public class MsgDaoTest extends DaoTest {
    @Autowired
    private MsgDao msgDao;
    @Autowired
    private AccountDao accountDao;

    @Transactional
    @Test
    public void getAllMsgsTest() throws Exception {
        List<PrivateMsg> msgs = msgDao.getAllMsgs();
        assertEquals(msgs, createMsgsForTest());
    }

    @Transactional
    @Test
    public void getAllAccountMsgsTest_firstAccount() throws Exception {
        List<PrivateMsg> msgs = msgDao.getAllAccountMsgs(createAccounts().get(0));
        assertEquals(msgs, createMsgsForTest());
    }

    @Transactional
    @Test
    public void getAllAccountMsgsTest_secondAccount() throws Exception {
        List<PrivateMsg> msgs = msgDao.getAllAccountMsgs(createAccounts().get(1));
        assertTrue(msgs.contains(createMsgsForTest().get(0)));
        assertTrue(msgs.contains(createMsgsForTest().get(1)));
        assertFalse(msgs.contains(createMsgsForTest().get(2)));
        assertFalse(msgs.contains(createMsgsForTest().get(3)));
    }

    @Transactional
    @Test
    public void getAllAccountMsgsTest_thirdAccount() throws Exception {
        List<PrivateMsg> msgs = msgDao.getAllAccountMsgs(createAccounts().get(2));
        assertFalse(msgs.contains(createMsgsForTest().get(0)));
        assertFalse(msgs.contains(createMsgsForTest().get(1)));
        assertTrue(msgs.contains(createMsgsForTest().get(2)));
        assertTrue(msgs.contains(createMsgsForTest().get(3)));
    }

    @Transactional
    @Test
    public void createMsgTest_viaMsgDaoCreate() throws Exception {
        Account firstAccount = accountDao.getEntityById(1);
        Account thirdAccount = accountDao.getEntityById(3);
        String msgText = "new message";
        PrivateMsg msg = new PrivateMsg(firstAccount, thirdAccount, msgText);
        msgDao.createMsg(msg);
        assertTrue(msgDao.getAllMsgs().contains(msg));
    }

    @Transactional
    @Test
    public void createMsgTest_viaAccountAddMsgSent() throws Exception {
        Account firstAccount = accountDao.getEntityById(1);
        Account thirdAccount = accountDao.getEntityById(3);
        String msgText = "new message";
        firstAccount.addMsqSent(thirdAccount, msgText);
        PrivateMsg msg = new PrivateMsg(firstAccount, thirdAccount, msgText);
        assertTrue(msgDao.getAllMsgs().contains(msg));
    }

    @Transactional
    @Test
    public void createMsgTest_viaAccountAddMsgReceived() throws Exception {
        Account firstAccount = accountDao.getEntityById(1);
        Account thirdAccount = accountDao.getEntityById(3);
        String msgText = "new message";
        thirdAccount.addMsgReceived(firstAccount, msgText);
        PrivateMsg msg = new PrivateMsg(firstAccount, thirdAccount, msgText);
        assertTrue(msgDao.getAllMsgs().contains(msg));
    }

    @Transactional
    @Test
    public void getAllMsgsOfPairTest() throws Exception {
        Account firstAccount = accountDao.getEntityById(1);
        Account secondAccount = accountDao.getEntityById(2);
        List<PrivateMsg> msgs = msgDao.getMsgsOfPair(firstAccount, secondAccount);
        assertTrue(msgs.contains(createMsgsForTest().get(0)));
        assertTrue(msgs.contains(createMsgsForTest().get(1)));
    }

    @Transactional
    @Test
    public void getAllMsgsOfPairTest_reversePair() throws Exception {
        Account firstAccount = accountDao.getEntityById(1);
        Account secondAccount = accountDao.getEntityById(2);
        List<PrivateMsg> msgs = msgDao.getMsgsOfPair(firstAccount, secondAccount);
        List<PrivateMsg> reverseMsgs = msgDao.getMsgsOfPair(secondAccount, firstAccount);
        assertEquals(msgs,reverseMsgs);
    }

    public List<PrivateMsg> createMsgsForTest() {
        List<PrivateMsg> privateMsgs = new LinkedList<>();
        List<Account> accounts = createAccounts();
        privateMsgs.add(new PrivateMsg(1, accounts.get(0), accounts.get(1), "First message from first to second"));
        privateMsgs.add(new PrivateMsg(2, accounts.get(1), accounts.get(0), "Response message from second to first"));
        privateMsgs.add(new PrivateMsg(3, accounts.get(0), accounts.get(2), "Second message from first to third"));
        privateMsgs.add(new PrivateMsg(4, accounts.get(2), accounts.get(0), "Response message from third to first"));
        return privateMsgs;
    }

}
