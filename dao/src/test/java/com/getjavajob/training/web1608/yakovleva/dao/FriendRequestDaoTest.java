package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.FriendRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:social-dao-context.xml", "classpath:social-dao-context-test.xml"})
public class FriendRequestDaoTest extends DaoTest {
    @Autowired
    private FriendRequestDao requestDao;
    @Autowired
    private AccountDao accountDao;

    private List<Account> accounts;

    @Before
    public void setUp() throws Exception {
        Account newAcc = accountDao.create(new Account("Vasya", "Titkin", "titkin@email.com", "123"));
        accounts = createAccounts();
        accounts.add(newAcc);
    }

    @Transactional
    @Test
    public void createRequestTest_addToAccountFrom() throws Exception {
        Account account4 = accountDao.getEntityById(4);
        Account account1 = accountDao.getEntityById(1);
//        FriendRequest friendRequest = requestDao.createFriendRequest(account4, account1);
        account4.addRequestsFromMe(account1);
        assertTrue(account4.getRequestedFromMe().contains(account1));
        assertTrue(account1.getRequestedToMe().contains(account4));
        assertNotNull(requestDao.getFriendRequest(account4, account1));
//        assertEquals(requestDao.getFriendRequest(account4, account1), friendRequest);
    }

    @Transactional
    @Test
    public void createRequestTest_addToAccountTo() throws Exception {
        Account account4 = accountDao.getEntityById(4);
        Account account1 = accountDao.getEntityById(1);
        account4.addRequestsToMe(account1);
        assertTrue(account4.getRequestedToMe().contains(account1));
        assertTrue(account1.getRequestedFromMe().contains(account4));
        assertNotNull(requestDao.getFriendRequest(account1, account4));
    }

    @Transactional
    @Test
    public void getAllRequestsTest() throws Exception {
        List<FriendRequest> requests = requestDao.getAllRequests();
        assertTrue(requests.contains(createRequests().get(0)));
        assertTrue(requests.contains(createRequests().get(1)));
        assertTrue(requests.contains(createRequests().get(2)));
    }

    @Transactional
    @Test
    public void getFriendRequestTest() throws Exception {
        FriendRequest request = requestDao.getFriendRequest(accountDao.getEntityById(2), accountDao.getEntityById(1));
        assertEquals(request, createRequests().get(1));
    }

    @Transactional
    @Test
    public void getAllAccountRequestsTest() throws Exception {
        Account account = accountDao.getEntityById(2);
        List<FriendRequest> requests = requestDao.getAllAccountRequests(account);
        System.out.println(requests);
        System.out.println(createRequests().get(0));
        assertFalse(requests.contains(createRequests().get(0)));
        assertTrue(requests.contains(createRequests().get(1)));
        assertTrue(requests.contains(createRequests().get(2)));
    }

    @Transactional
    @Test
    public void acceptRequestTest() throws Exception {
        Account account2 = accountDao.getEntityById(2);
        Account account1 = accountDao.getEntityById(1);
        assertFalse(requestDao.getFriendRequest(account2, account1).isAccepted());
        requestDao.acceptRequest(account2, account1);
        assertTrue(requestDao.getFriendRequest(account2, account1).isAccepted());
    }

    @Transactional
    @Test
    public void rejectRequestTest() throws Exception {
        Account account1 = accountDao.getEntityById(1);
        Account account3 = accountDao.getEntityById(3);
        assertTrue(requestDao.getFriendRequest(account1, account3).isAccepted());
        requestDao.rejectRequest(account1, account3);
        assertFalse(requestDao.getFriendRequest(account1, account3).isAccepted());
    }

    @Transactional
    @Test
    public void removeRequestTest() throws Exception {
        Account account1 = accountDao.getEntityById(1);
        Account account3 = accountDao.getEntityById(3);
        assertNotNull(requestDao.getFriendRequest(account1, account3));
        requestDao.removeRequest(account1, account3);
        assertNull(requestDao.getFriendRequest(account1, account3));
    }
}
