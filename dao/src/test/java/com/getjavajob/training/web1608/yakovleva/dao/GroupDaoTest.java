package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.dao.utils.DaoException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:social-dao-context.xml", "classpath:social-dao-context-test.xml"})
public class GroupDaoTest extends DaoTest {
    @Autowired
    private GroupDao dao;

    @Transactional
    @Test
    public void getEntityByIdTest() throws Exception {
        Group group = dao.getEntityById(1);
        assertEquals(group, createGroups().get(0));
        group = dao.getEntityById(2);
        assertEquals(group, createGroups().get(1));
        group = dao.getEntityById(3);
        assertEquals(group, createGroups().get(2));
    }

    @Transactional
    @Test
    public void getEntityByWrongIdTest() throws Exception {
        assertNull(dao.getEntityById(5));
    }

    @Transactional
    @Test
    public void getAllTest() throws Exception {
        List<Group> testGroups = dao.getAll();
        List<Group> actualGroups = createGroups();
        assertEquals(testGroups.get(0), actualGroups.get(0));
        assertEquals(testGroups.get(1), actualGroups.get(1));
        assertEquals(testGroups.get(2), actualGroups.get(2));
    }

    @Transactional
    @Test
    public void updateTest() throws Exception {
        Group group = dao.getEntityById(3);
        group.setGroupName("Java Developers");
        dao.update(group);
        assertEquals(dao.getEntityById(3).getGroupName(), "Java Developers");
    }

    @Transactional
    @Test
    public void createTest() throws Exception {
        Group group = new Group(4);
        group.setGroupName("Java Developers");
        group.setCreator(new Account(1, "Alexander", "Yakovlev", "yakovlevas@gmail.com", "qwerty"));
        List<Group> groups = createGroups();
        groups.add(dao.create(group));
        assertEquals(group, dao.getEntityById(4));
    }

    @Transactional
    @Test(expected = DaoException.class)
    public void createBadGroupTest() throws Exception {
        Group group = new Group(4);
        dao.create(group);
        assertEquals(group, dao.getEntityById(4));
    }

    @Transactional
    @Test
    public void createExistentGroupTest() throws Exception {
        Group group = new Group(2);
        group.setGroupName("getJavaJob Algo");
        group.setCreator(new Account(1, "Alexander", "Yakovlev", "yakovlevas@gmail.com", "qwerty"));
        dao.create(group);
        assertEquals(group, dao.getEntityById(2));
    }

    @Transactional
    @Test
    public void groupMembersTest() throws Exception {
        List<Account> members = dao.getEntityById(2).getMembers();
        assertTrue(members.contains(createAccounts().get(0)));
        assertFalse(members.contains(createAccounts().get(1)));
        assertTrue(members.contains(createAccounts().get(2)));
    }

    @Transactional
    @Test
    public void groupOwnerTest() throws Exception {
        assertEquals(dao.getEntityById(3).getCreator(), createAccounts().get(1));
    }

    @Transactional
    @Test
    public void getEntityByPatternTest_getFirst() throws Exception {
        List<Group> accounts = dao.getEntitiesByPattern("get",0,1);
        assertEquals(accounts.get(0), createGroups().get(0));
        assertEquals(accounts.size(), 1);
    }

    @Transactional
    @Test
    public void getEntityByPatternTest_getTwo() throws Exception {
        List<Group> accounts = dao.getEntitiesByPattern("get",1,2);
        assertEquals(accounts.size(), 2);
        assertTrue(accounts.contains(createGroups().get(1)));
        assertTrue(accounts.contains(createGroups().get(2)));
    }

    @Transactional
    @Test
    public void getSearchQuerySizeTest() throws Exception {
        assertEquals(dao.getSearchEntitiesCount("Web"), 1);
        assertEquals(dao.getSearchEntitiesCount("get"), 3);
    }

    @Transactional
    @Test
    public void getGroupByNameTest() throws Exception {
        assertNotNull(dao.getGroupByName("getJavaJob Web"));
    }
}
