package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.post.AccountPost;
import com.getjavajob.training.web1608.yakovleva.dao.post.AccountPostDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:social-dao-context.xml", "classpath:social-dao-context-test.xml"})
public class PostDaoTest extends DaoTest {
    @Autowired
    private AccountPostDao postDao;

    @Transactional
    @Test
    public void createAccountPost_checkIdCreation() throws Exception {
        List<Account> accounts = createAccounts();
        AccountPost newPost = new AccountPost(accounts.get(0), accounts.get(1), "Some post");
        AccountPost accountPost = postDao.createPost(newPost);
        assertNotEquals(0, accountPost.getId());
    }

    @Transactional
    @Test
    public void getByIdPostTest() throws Exception {
        assertEquals(postDao.getPostById(1), createAccountPosts().get(0));
    }

    @Transactional
    @Test
    public void createAccountPost_checkCreatedById() throws Exception {
        List<Account> accounts = createAccounts();
        AccountPost newPost = new AccountPost(accounts.get(0), accounts.get(1), "Some post");
        AccountPost accountPost = postDao.createPost(newPost);
        assertNotNull(postDao.getPostById(accountPost.getId()));
    }

    @Transactional
    @Test
    public void getAllEntityPostsTest() throws Exception {
        List<AccountPost> posts = postDao.getAllEntityPosts(createAccounts().get(0));
        assertTrue(posts.contains(createAccountPosts().get(0)));
        assertTrue(posts.contains(createAccountPosts().get(1)));
        assertTrue(posts.contains(createAccountPosts().get(2)));
        assertFalse(posts.contains(createAccountPosts().get(3)));
    }

    @Transactional
    @Test
    public void deletePostTest() throws Exception {
        assertNotNull(postDao.getPostById(1));
        postDao.deletePostById(1);
        assertNull(postDao.getPostById(1));
    }
}
