package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.FriendRequest;
import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.common.post.AccountPost;
import com.getjavajob.training.web1608.yakovleva.common.post.GroupPost;
import org.springframework.test.context.jdbc.Sql;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Sql(value = {"classpath:CreateTables.sql", "classpath:FillTables.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:DropSchema.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class DaoTest {
    protected static List<Account> createAccounts() {
        Account account1 = new Account(1, "Alexander", "Yakovlev", "yakovlevas@gmail.com", "querty");
        try {
            account1.setBirthday(new SimpleDateFormat("yyyy-MM-dd").parse("1990-07-30"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Account account2 = new Account(2, "Emin", "Mamedov", "mamedov@gmail.com", "querty");
        Account account3 = new Account(3, "Vitaly", "Severin", "severin@gmail.com", "querty");
        ArrayList<Account> accounts = new ArrayList<>(3);
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;
    }

    protected static List<Group> createGroups() {
        Account account1 = createAccounts().get(0);
        Account account2 = createAccounts().get(1);
        Account account3 = createAccounts().get(2);

        ArrayList<Group> groups = new ArrayList<>(3);
        Group firstGroup = new Group(1);
        firstGroup.setGroupName("getJavaJob Init");
        firstGroup.setCreator(account3);
        firstGroup.addMember(account2);
        firstGroup.addMember(account3);
        groups.add(firstGroup);

        Group secondGroup = new Group(2);
        secondGroup.setGroupName("getJavaJob Algo");
        secondGroup.setCreator(account3);
        secondGroup.addMember(account1);
        groups.add(secondGroup);

        Group thirdGroup = new Group(3);
        thirdGroup.setGroupName("getJavaJob Web");
        thirdGroup.setCreator(account2);
        groups.add(thirdGroup);
        return groups;
    }

    protected static List<FriendRequest> createRequests() {
        Account account1 = createAccounts().get(0);
        Account account2 = createAccounts().get(1);
        Account account3 = createAccounts().get(2);

        List<FriendRequest> requests = new LinkedList<>();
        requests.add(new FriendRequest(account1,account3,true));
        requests.add(new FriendRequest(account2,account1));
        requests.add(new FriendRequest(account2,account3,true));
        return requests;
    }

    protected static List<AccountPost> createAccountPosts() {
        List<Account> accounts = createAccounts();

        List<AccountPost> posts = new LinkedList<>();
        posts.add(new AccountPost(accounts.get(1),accounts.get(0), "Post from second account"));
        posts.add(new AccountPost(accounts.get(0),accounts.get(0), "Self post"));
        posts.add(new AccountPost(accounts.get(2),accounts.get(0), "Post from third account"));
        posts.add(new AccountPost(accounts.get(0),accounts.get(2), "Post from first account"));
        return posts;
    }

    protected static List<GroupPost> ceateGroupPosts() {
        List<Account> accounts = createAccounts();
        List<Group> groups = createGroups();

        List<GroupPost> posts = new LinkedList<>();
        posts.add(new GroupPost(accounts.get(1),groups.get(0), "Post from second account"));
        posts.add(new GroupPost(accounts.get(0),groups.get(1), "Group post from first account to third group"));
        posts.add(new GroupPost(accounts.get(2),groups.get(2), "Group Post from third account"));
        posts.add(new GroupPost(accounts.get(0),groups.get(2), "Group Post from first account"));
        return posts;
    }
}
