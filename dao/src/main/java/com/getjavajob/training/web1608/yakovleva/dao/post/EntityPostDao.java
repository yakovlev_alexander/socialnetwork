package com.getjavajob.training.web1608.yakovleva.dao.post;

import com.getjavajob.training.web1608.yakovleva.common.SNEntity;
import com.getjavajob.training.web1608.yakovleva.common.post.EntityPost;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public abstract class EntityPostDao<T extends SNEntity, E extends EntityPost<T>> {
    @PersistenceContext
    private EntityManager entityManager;

    protected abstract CriteriaQuery<E> buildCriteriaQuery(CriteriaBuilder builder);

    protected abstract Root<E> createRoot(CriteriaQuery<E> criteriaQuery);

    public List<E> getAllEntityPosts(T entity) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = buildCriteriaQuery(builder);
        Root<E> requestRoot = createRoot(query);
        try {
            query.select(requestRoot)
                    .where(builder.equal(requestRoot.get("toEntity"), entity.getId()))
                    .orderBy(builder.desc(requestRoot.get("sendingDate")));
            return entityManager.createQuery(query).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public E createPost(E newPost) {
        return entityManager.merge(newPost);
    }

    public E getPostById(long id) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = buildCriteriaQuery(builder);
        Root<E> requestRoot = createRoot(query);
        try {
            query.select(requestRoot)
                    .where(builder.equal(requestRoot.get("id"), id));
            return entityManager.createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void deletePostById(long id) {
        entityManager.remove(getPostById(id));
    }
}
