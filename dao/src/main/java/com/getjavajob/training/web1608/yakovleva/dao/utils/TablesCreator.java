package com.getjavajob.training.web1608.yakovleva.dao.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class TablesCreator {
    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException, DaoException {
        createTables("mysql.properties");
    }

    public static void createTables(String properties) throws SQLException, IOException, DaoException {
        try (Connection connection = SqlUtils.createConnection(properties)) {
            SqlUtils.executeSql(connection, "CreateTables.sql");
            SqlUtils.executeSql(connection, "FillTables.sql");
        }
    }
}
