package com.getjavajob.training.web1608.yakovleva.dao.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class SchemaDropper {
    public static void main(String[] args) throws SQLException, ClassNotFoundException, DaoException {
        dropSchema("mysql.properties");
    }

    public static void dropSchema(String properties) throws SQLException, DaoException {
        try (Connection connection = SqlUtils.createConnection(properties)) {
            SqlUtils.executeSql(connection, "DropSchema.sql");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
