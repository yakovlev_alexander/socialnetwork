package com.getjavajob.training.web1608.yakovleva.dao.post;

import com.getjavajob.training.web1608.yakovleva.common.Group;
import com.getjavajob.training.web1608.yakovleva.common.post.GroupPost;
import org.springframework.stereotype.Repository;

import javax.persistence.OneToMany;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class GroupPostDao extends EntityPostDao<Group, GroupPost> {
    @Override
    protected CriteriaQuery<GroupPost> buildCriteriaQuery(CriteriaBuilder builder) {
        return builder.createQuery(GroupPost.class);
    }

    @Override
    protected Root<GroupPost> createRoot(CriteriaQuery<GroupPost> criteriaQuery) {
        return criteriaQuery.from(GroupPost.class);
    }
}
