package com.getjavajob.training.web1608.yakovleva.dao.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SqlUtils {
    public static void executeSql(Connection connection, String queriesFileName) throws SQLException, IOException {
        try (BufferedReader bf = new BufferedReader(new InputStreamReader(
                ClassLoader.getSystemResourceAsStream(queriesFileName)))) {
            StringBuilder sb = new StringBuilder();
            while (bf.ready()) {
                sb.append(bf.readLine().trim());
                if (sb.toString().endsWith(";")) {
                    connection.createStatement().execute(sb.toString());
                    sb = new StringBuilder();
                }
            }
        }
    }

    public static Connection createConnection(String propFile) throws DaoException {
        try {
            Properties prop = loadPropertiesFromFile(propFile);
            String driver = prop.getProperty("database.driver");
            String url = prop.getProperty("database.url");
            String user = prop.getProperty("database.user");
            String pass = prop.getProperty("database.password");
            Class.forName(driver);
            Connection connection = DriverManager.getConnection(url, user, pass);
            return connection;
        } catch (ClassNotFoundException | SQLException e) {
            throw new DaoException("Unable to create Connection", e);
        }
    }

    private static Properties loadPropertiesFromFile(String propFile) {
        Properties prop = new Properties();
        try {
            prop.load(TablesCreator.class.getClassLoader().getResourceAsStream(propFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
