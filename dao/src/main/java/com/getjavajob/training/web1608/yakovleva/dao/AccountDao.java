package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Repository
public class AccountDao extends AbstractDao<Account> {
    @Override
    protected CriteriaQuery<Account> getEntityQuery(CriteriaBuilder builder) {
        return builder.createQuery(Account.class);
    }

    @Override
    protected Root<Account> getEntityRoot(CriteriaQuery<?> query) {
        return query.from(Account.class);
    }

    @Override
    protected Predicate getSearchWherePredicate(String pattern, CriteriaBuilder builder, Root<Account> root) {
        return builder.or(
                builder.like(builder.lower(root.get("name")), "%" + pattern.toLowerCase() + "%"),
                builder.like(builder.lower(root.get("familyName")), "%" + pattern.toLowerCase() + "%")
        );
    }

    public Account getAccountByEmail(String email) {
        if (email == null) {
            return null;
        }
        try {
            CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Account> query = getEntityQuery(builder);
            Root<Account> accountRoot = getEntityRoot(query);
            query.select(accountRoot).where(builder.equal(accountRoot.get("email"), email));
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
