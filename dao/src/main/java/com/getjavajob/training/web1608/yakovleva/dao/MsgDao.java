package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.PrivateMsg;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class MsgDao {
    @PersistenceContext
    private EntityManager entityManager;

    public List<PrivateMsg> getAllMsgs() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PrivateMsg> query = builder.createQuery(PrivateMsg.class);
        Root<PrivateMsg> requestRoot = query.from(PrivateMsg.class);
        query.select(requestRoot);
        return entityManager.createQuery(query).getResultList();
    }

    public List<PrivateMsg> getMsgsOfPair(Account from, Account to) {
        try {
            CriteriaBuilder builder = entityManager.getCriteriaBuilder();
            CriteriaQuery<PrivateMsg> query = builder.createQuery(PrivateMsg.class);
            Root<PrivateMsg> requestRoot = query.from(PrivateMsg.class);
            query.select(requestRoot).where(
                    builder.or(
                            builder.and(
                                    builder.equal(requestRoot.get("fromAccount"), from.getId()),
                                    builder.equal(requestRoot.get("toAccount"), to.getId())
                            ), builder.and(
                                    builder.equal(requestRoot.get("fromAccount"), to.getId()),
                                    builder.equal(requestRoot.get("toAccount"), from.getId())
                            )
                    )
            );
            return entityManager.createQuery(query).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<PrivateMsg> getAllAccountMsgs(Account account) {
        long accId = account.getId();
        return entityManager.createNamedQuery("findAllAccountMsgs", PrivateMsg.class)
                .setParameter("from", accId)
                .setParameter("to", accId).getResultList();
    }

    public PrivateMsg createMsg(PrivateMsg newMsg) {
        return entityManager.merge(newMsg);
    }
}
