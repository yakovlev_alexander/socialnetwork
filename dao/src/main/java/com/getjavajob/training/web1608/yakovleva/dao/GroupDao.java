package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.Group;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.*;

@Repository
public class GroupDao extends AbstractDao<Group> {
    @Override
    protected CriteriaQuery<Group> getEntityQuery(CriteriaBuilder builder) {
        return builder.createQuery(Group.class);
    }

    @Override
    protected Root<Group> getEntityRoot(CriteriaQuery<?> query) {
        return query.from(Group.class);
    }

    @Override
    protected Predicate getSearchWherePredicate(String pattern, CriteriaBuilder builder, Root<Group> root) {
        return builder.like(builder.lower(root.get("groupName")), "%" + pattern.toLowerCase() + "%");
    }

    public Group getGroupByName(String name) {
        if (name == null) {
            return null;
        }
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Group> query = getEntityQuery(builder);
        Root<Group> root = getEntityRoot(query);
        query.select(root).where(builder.equal(root.get("groupName"), name));
        try {
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
