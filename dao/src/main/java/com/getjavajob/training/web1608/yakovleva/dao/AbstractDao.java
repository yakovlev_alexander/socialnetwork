package com.getjavajob.training.web1608.yakovleva.dao;

import com.getjavajob.training.web1608.yakovleva.common.SNEntity;
import com.getjavajob.training.web1608.yakovleva.dao.utils.DaoException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Repository
public abstract class AbstractDao<E extends SNEntity> {
    @PersistenceContext
    private EntityManager entityManager;

    protected abstract CriteriaQuery<E> getEntityQuery(CriteriaBuilder builder);

    protected abstract Root<E> getEntityRoot(CriteriaQuery<?> query);

    protected abstract Predicate getSearchWherePredicate(String pattern, CriteriaBuilder builder, Root<E> root);

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    public void update(E entity) {
        if (entity == null) {
            return;
        }
        getEntityById(entity.getId()).updateEntity(entity);
    }

    public List<E> getAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = getEntityQuery(builder);
        Root<E> groupRoot = getEntityRoot(query);
        query.select(groupRoot);
        return getEntityManager().createQuery(query).getResultList();
    }

    public E getEntityById(long id) throws EmptyResultDataAccessException {
        if (id <= 0) {
            return null;
        }
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = getEntityQuery(builder);
        Root<E> root = getEntityRoot(query);
        query.select(root).where(builder.equal(root.get("id"), id));
        try {
            return getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<E> getEntitiesByPattern(String pattern, int firstRow, int size) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = getEntityQuery(builder);
        Root<E> root = getEntityRoot(query);
        query.select(root).where(
                getSearchWherePredicate(pattern, builder, root)
        );
        return getEntityManager().createQuery(query).setFirstResult(firstRow).setMaxResults(size).getResultList();
    }

    public long getSearchEntitiesCount(String pattern) {
        CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<E> accountRoot = getEntityRoot(query);
        query.select(builder.count(accountRoot)).where(
                getSearchWherePredicate(pattern, builder, accountRoot)
        );
        return getEntityManager().createQuery(query).getSingleResult();
    }

    public E create(E entity) throws DaoException {
        if (entity == null) {
            return null;
        }
        try {
            return getEntityManager().merge(entity);
        } catch (PersistenceException e) {
            throw new DaoException("Non-null field of inserted object is null.", e);
        }
    }
}
