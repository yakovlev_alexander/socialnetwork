package com.getjavajob.training.web1608.yakovleva.dao.post;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.post.AccountPost;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
public class AccountPostDao extends EntityPostDao<Account, AccountPost> {
    @Override
    protected CriteriaQuery<AccountPost> buildCriteriaQuery(CriteriaBuilder builder) {
        return builder.createQuery(AccountPost.class);
    }

    @Override
    protected Root<AccountPost> createRoot(CriteriaQuery<AccountPost> criteriaQuery) {
        return criteriaQuery.from(AccountPost.class);
    }
}
