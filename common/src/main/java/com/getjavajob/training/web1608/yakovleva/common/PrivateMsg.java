package com.getjavajob.training.web1608.yakovleva.common;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllAccountMsgs",
                query = "SELECT pm FROM PrivateMsg pm WHERE fromAccount.id = :from OR toAccount.id = :to")
})
@Table(name = "msg_tbl")
public class PrivateMsg implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "SENDER_ID")
    private Account fromAccount;
    @ManyToOne
    @JoinColumn(name = "RECIPIENT_ID")
    private Account toAccount;

    @Column(name = "MSG_TEXT")
    private String msgText;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SENDING_DATE")
    private Date sendingDate;

    public PrivateMsg() {
    }

    public PrivateMsg(Account fromAccount, Account toAccount, String msgText) {
        setFromAccount(fromAccount);
        setToAccount(toAccount);
        setMsgText(msgText);
    }

    public PrivateMsg(long id, Account fromAccount, Account toAccount, String msgText) {
        this(fromAccount, toAccount, msgText);
        this.id = id;
    }

    @PrePersist
    protected void onCreate() {
        sendingDate = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account sender) {
        this.fromAccount = sender;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account recipient) {
        this.toAccount = recipient;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msdText) {
        this.msgText = msdText;
    }

    public Date getSendingDate() {
        return sendingDate;
    }

    public void setSendingDate(Date sendingDate) {
        this.sendingDate = sendingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PrivateMsg msg = (PrivateMsg) o;

        return toAccount.getId() == msg.toAccount.getId()
                && fromAccount.getId() == msg.fromAccount.getId()
                && msgText != null ? msgText.equals(msg.msgText) : msg.msgText == null;

    }

    @Override
    public int hashCode() {
        int result = fromAccount.hashCode();
        result = 31 * result + toAccount.hashCode();
        result = 31 * result + (msgText != null ? msgText.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PrivateMsg{" +
                "id=" + id +
                ", fromAccount=" + fromAccount.getId() +
                ", toAccount=" + toAccount.getId() +
                ", msdText='" + msgText + '\'' +
                '}';
    }
}
