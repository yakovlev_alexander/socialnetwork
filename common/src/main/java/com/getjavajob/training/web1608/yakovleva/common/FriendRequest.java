package com.getjavajob.training.web1608.yakovleva.common;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllAccountRequests",
                query = "SELECT f FROM FriendRequest f WHERE fromAccount.id = :from OR toAccount.id = :to")
})
@Table(name = "friends_tbl")
public class FriendRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private Account fromAccount;
    @Id
    @ManyToOne
    @JoinColumn(name = "FRIEND_ID")
    private Account toAccount;
    @Column(name = "RESPONSE", nullable = false)
    private boolean accepted;

    public FriendRequest() {
    }

    public FriendRequest(Account fromAccount, Account toAccount) {
        setToAccount(toAccount);
        setFromAccount(fromAccount);
    }

    public FriendRequest(Account fromAccount, Account toAccount, boolean accepted) {
        this(fromAccount, toAccount);
        this.accepted = accepted;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean status) {
        this.accepted = status;
    }

    @Override
    public String toString() {
        return "FriendRequest{" +
                "fromAccount=" + (fromAccount != null ? String.valueOf(fromAccount.getId()) : null) +
                ", toAccount=" + (toAccount != null ? String.valueOf(toAccount.getId()) : null) +
                ", accepted=" + accepted +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FriendRequest request = (FriendRequest) o;
        if (request.getFromAccount() == null || request.getToAccount() == null
                || this.getFromAccount() == null || this.getToAccount() == null ) {
            return false;
        }
        return toAccount.getId() == request.toAccount.getId()
                && fromAccount.getId() == request.fromAccount.getId()
                && accepted == request.accepted;
    }

    @Override
    public int hashCode() {
        int result = fromAccount != null ? fromAccount.hashCode() : 0;
        result = 31 * result + (toAccount != null ? toAccount.hashCode() : 0);
        result = 31 * result + (accepted ? 1 : 0);
        return result;
    }
}
