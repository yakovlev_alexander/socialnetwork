package com.getjavajob.training.web1608.yakovleva.common;

import java.io.Serializable;
import java.util.Date;

public interface SNEntity extends Serializable {
    long getId();

    void setId(long id);

    Date getRegistrationDate();

    void setRegistrationDate(Date registrationDate);

    boolean isActive();

    void setActive(boolean active);

    byte[] getPicture();

    void setPicture(byte[] picture);

    void updateEntity(SNEntity entity);
}
