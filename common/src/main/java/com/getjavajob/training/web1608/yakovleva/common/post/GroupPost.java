package com.getjavajob.training.web1608.yakovleva.common.post;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.Group;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "group_post_tbl")
public class GroupPost implements EntityPost<Group> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "SENDER_ID")
    private Account fromAccount;
    @ManyToOne
    @JoinColumn(name = "RECIPIENT_ID")
    private Group toEntity;

    @Column(name = "MSG_TEXT")
    private String msgText;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SENDING_DATE")
    private Date sendingDate;

    public GroupPost() {
    }

    public GroupPost(Account fromAccount, Group toEntity, String msgText) {
        this.fromAccount = fromAccount;
        this.toEntity = toEntity;
        this.msgText = msgText;
    }

    @PrePersist
    protected void onCreate() {
        sendingDate = new Date();
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Account getFromAccount() {
        return fromAccount;
    }

    @Override
    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    @Override
    public Group getToEntity() {
        return toEntity;
    }

    @Override
    public void setToEntity(Group toEntity) {
        this.toEntity = toEntity;
    }

    @Override
    public String getMsgText() {
        return msgText;
    }

    @Override
    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    @Override
    public Date getSendingDate() {
        return sendingDate;
    }

    @Override
    public void setSendingDate(Date sendingDate) {
        this.sendingDate = sendingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupPost groupPost = (GroupPost) o;
        if (!fromAccount.equals(groupPost.fromAccount)) {
            return false;
        }
        if (!toEntity.equals(groupPost.toEntity)) {
            return false;
        }
        return msgText != null ? msgText.equals(groupPost.msgText) : groupPost.msgText == null;
    }

    @Override
    public int hashCode() {
        int result = fromAccount.hashCode();
        result = 31 * result + toEntity.hashCode();
        result = 31 * result + (msgText != null ? msgText.hashCode() : 0);
        return result;
    }
}
