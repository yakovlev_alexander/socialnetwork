package com.getjavajob.training.web1608.yakovleva.common;

public enum EntityType {
    ACCOUNT("account"), GROUP("group");

    String lowerCase;

    EntityType(String lowerCase) {
        this.lowerCase = lowerCase;
    }

    @Override
    public String toString() {
        return lowerCase;
    }
}
