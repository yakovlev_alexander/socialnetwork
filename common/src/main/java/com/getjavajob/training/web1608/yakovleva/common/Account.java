package com.getjavajob.training.web1608.yakovleva.common;

import com.thoughtworks.xstream.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@XStreamAlias("Account")
@Entity
@Table(name = "account_tbl")
public class Account implements SNEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XStreamAsAttribute
    private long id;

    @Column(nullable = false)
    private String name;
    @Column(name = "FAMILY_NAME", nullable = false)
    private String familyName;
    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTH_DAY")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @Column(name = "PERSONAL_PHONE")
    private String personalPhone;
    @Column(name = "WORK_PHONE")
    private String workPhone;
    @Column(unique = true)
    private String email;
    @XStreamOmitField
    private String pass;
    private String icq;
    private String skype;
    @Column(name = "ADD_INFO")
    private String addInfo;
    @XStreamOmitField
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REGISTRATION_DATE")
    private Date registrationDate;
    @XStreamOmitField
    @Column(nullable = false)
    private boolean active;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "skills_tbl", joinColumns = @JoinColumn(name = "ACCOUNT_ID"))
    @XStreamImplicit(itemFieldName="Skill")
    @Column(name = "SKILL")
    private List<String> skills;
    @XStreamOmitField
    @Column(name = "PIC")
    private byte[] picture;

    @XStreamOmitField
    @OneToMany(mappedBy = "fromAccount", cascade = CascadeType.ALL)
    private List<FriendRequest> requestsFromMe;
    @XStreamOmitField
    @OneToMany(mappedBy = "toAccount", cascade = CascadeType.ALL)
    private List<FriendRequest> requestsToMe;

    @XStreamOmitField
    @OneToMany(mappedBy = "fromAccount", cascade = CascadeType.ALL)
    private List<PrivateMsg> msgsSent;
    @XStreamOmitField
    @OneToMany(mappedBy = "toAccount", cascade = CascadeType.ALL)
    private List<PrivateMsg> msgsRecieved;

    @XStreamOmitField
    @ManyToMany
    @JoinTable(name = "group_members_tbl",
            joinColumns = @JoinColumn(name = "MEMBER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID"))
    private List<Group> accountGroups;

    public Account() {
        requestsFromMe = new LinkedList<>();
        requestsToMe = new LinkedList<>();
        accountGroups = new LinkedList<>();
        skills = new LinkedList<>();

        msgsRecieved = new LinkedList<>();
        msgsSent = new LinkedList<>();
    }

    public Account(long id, String name, String familyName, String email, String pass) {
        this(name, familyName, email, pass);
        this.id = id;
        active = true;
    }

    public Account(String name, String familyName, String email, String pass) {
        this();
        this.name = name;
        this.familyName = familyName;
        this.email = email;
        this.pass = pass;
        active = true;
    }

    @PrePersist
    protected void onCreate() {
        registrationDate = new Date();
    }

    @Override
    public void updateEntity(SNEntity entity) {
        Account account = (Account) entity;
        this.name = account.name;
        this.familyName = account.familyName;
        this.middleName = account.middleName;
        this.birthday = account.birthday;
        this.personalPhone = account.personalPhone;
        this.workPhone = account.workPhone;
        this.email = account.email;
        this.pass = account.pass;
        this.icq = account.icq;
        this.skype = account.skype;
        this.addInfo = account.addInfo;
        this.active = account.active;
        this.skills = account.skills;
        this.picture = account.picture;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public Date getRegistrationDate() {
        return registrationDate;
    }

    @Override
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public byte[] getPicture() {
        return picture;
    }

    @Override
    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPersonalPhone() {
        return personalPhone;
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public List<Group> getAccountGroups() {
        return accountGroups;
    }

    public void setAccountGroups(List<Group> accountGroups) {
        this.accountGroups = accountGroups;
    }

    public void addGroup(Group group) {
        accountGroups.add(group);
        if(!group.getMembers().contains(this)) {
            group.addMember(this);
        }
    }

    public List<Account> getFriends() {
        List<Account> friends = new LinkedList<>();
        List<FriendRequest> requests = requestsToMe;
        requests.addAll(requestsFromMe);
        for (FriendRequest request : requests) {
            if(request.getFromAccount().equals(this) && request.isAccepted()) {
                friends.add(request.getToAccount());
            } else if(request.getToAccount().equals(this) && request.isAccepted()) {
                friends.add(request.getFromAccount());
            }
        }
        return friends;
    }

    public List<Account> getRequestedToMe() {
        List<Account> followers = new LinkedList<>();
        for (FriendRequest request : requestsToMe) {
            if(request.getToAccount().equals(this) && !request.isAccepted()) {
                followers.add(request.getFromAccount());
            }
        }
        return followers;
    }

    public List<Account> getRequestedFromMe() {
        List<Account> followed = new LinkedList<>();
        for (FriendRequest request : requestsFromMe) {
            if(request.getFromAccount().equals(this) && !request.isAccepted()) {
                followed.add(request.getToAccount());
            }
        }
        return followed;
    }

    public void setRequestsFromMe(List<FriendRequest> requestsFromMe) {
        this.requestsFromMe = requestsFromMe;
    }

    public void addRequestsFromMe(Account to) {
        FriendRequest newRequest = new FriendRequest(this, to);
        this.requestsFromMe.add(newRequest);
        to.requestsToMe.add(newRequest);
    }

    public void setRequestsToMe(List<FriendRequest> requestsToMe) {
        this.requestsToMe = requestsToMe;
    }

    public void addRequestsToMe(Account from) {
        FriendRequest newRequest = new FriendRequest(from, this);
        this.requestsToMe.add(newRequest);
        from.requestsFromMe.add(newRequest);
    }

    public List<FriendRequest> getRequestsFromMe() {
        return requestsFromMe;
    }

    public List<FriendRequest> getRequestsToMe() {
        return requestsToMe;
    }

    public List<PrivateMsg> getMsgsRecieved() {
        return msgsRecieved;
    }

    public void setMsgsRecieved(List<PrivateMsg> msgRecieved) {
        this.msgsRecieved = msgRecieved;
    }

    public List<PrivateMsg> getMsgsSent() {
        return msgsSent;
    }

    public void setMsgsSent(List<PrivateMsg> msgSent) {
        this.msgsSent = msgSent;
    }

    public void addMsqSent(Account to, String msgText) {
        PrivateMsg msg = new PrivateMsg(this, to, msgText);
        this.msgsSent.add(msg);
        to.msgsRecieved.add(msg);
    }

    public void addMsgReceived(Account from, String msgText) {
        PrivateMsg msg = new PrivateMsg(from, this, msgText);
        this.msgsRecieved.add(msg);
        from.msgsSent.add(msg);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", familyName='" + familyName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthday=" + birthday +
                ", personalPhone='" + personalPhone + '\'' +
                ", workPhone='" + workPhone + '\'' +
                ", email='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", addInfo='" + addInfo + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return email != null ? email.equals(account.email) : account.email == null;
    }

    @Override
    public int hashCode() {
        return email != null ? email.hashCode() : 0;
    }
}
