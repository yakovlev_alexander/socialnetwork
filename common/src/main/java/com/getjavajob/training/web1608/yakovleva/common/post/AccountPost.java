package com.getjavajob.training.web1608.yakovleva.common.post;

import com.getjavajob.training.web1608.yakovleva.common.Account;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "account_post_tbl")
public class AccountPost implements EntityPost<Account> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "SENDER_ID")
    private Account fromAccount;
    @ManyToOne
    @JoinColumn(name = "RECIPIENT_ID")
    private Account toEntity;

    @Column(name = "MSG_TEXT")
    private String msgText;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SENDING_DATE")
    private Date sendingDate;

    public AccountPost() {
    }

    public AccountPost(Account fromAccount, Account toEntity, String msgText) {
        this.fromAccount = fromAccount;
        this.toEntity = toEntity;
        this.msgText = msgText;
    }

    @PrePersist
    protected void onCreate() {
        sendingDate = new Date();
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Account getFromAccount() {
        return fromAccount;
    }

    @Override
    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    @Override
    public Account getToEntity() {
        return toEntity;
    }

    @Override
    public void setToEntity(Account toEntity) {
        this.toEntity = toEntity;
    }

    @Override
    public String getMsgText() {
        return msgText;
    }

    @Override
    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    @Override
    public Date getSendingDate() {
        return sendingDate;
    }

    @Override
    public void setSendingDate(Date sendingDate) {
        this.sendingDate = sendingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountPost that = (AccountPost) o;
        if (!fromAccount.equals(that.fromAccount)) {
            return false;
        }
        if (!toEntity.equals(that.toEntity)) {
            return false;
        }
        return msgText != null ? msgText.equals(that.msgText) : that.msgText == null;
    }

    @Override
    public int hashCode() {
        int result = fromAccount.hashCode();
        result = 31 * result + toEntity.hashCode();
        result = 31 * result + (msgText != null ? msgText.hashCode() : 0);
        return result;
    }
}
