package com.getjavajob.training.web1608.yakovleva.common;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Date;

@Entity
@Table(name = "group_tbl")
public class Group implements SNEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "GROUP_NAME", nullable = false)
    private String groupName;
    @OneToOne
    @JoinColumn(name = "OWNER", referencedColumnName = "ID", nullable = false)
    private Account creator;
    @ManyToMany(mappedBy = "accountGroups")
    private List<Account> members;
    @Column(name = "ADD_INFO")
    private String addInfo;
    @Column(name = "PIC")
    private byte[] picture;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REGISTRATION_DATE", columnDefinition = "timestamp without time zone DEFAULT now()")
    private Date registrationDate;
    @Column(nullable = false)
    private boolean active;

    public Group() {
        this.members = new LinkedList<>();
    }

    public Group(long id) {
        this();
        this.id = id;
    }

    @PrePersist
    protected void onCreate() {
        registrationDate = new Date();
    }

    @Override
    public void updateEntity(SNEntity entity) {
        Group group = (Group) entity;
        this.groupName = group.groupName;
        this.addInfo = group.addInfo;
        this.picture = group.picture;
        this.active = group.active;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getRegistrationDate() {
        return registrationDate;
    }

    @Override
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public byte[] getPicture() {
        return picture;
    }

    @Override
    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public List<Account> getMembers() {
        return members;
    }

    public void addMember(Account member) {
        members.add(member);
        if(!member.getAccountGroups().contains(this)) {
            member.addGroup(this);
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setMembers(List<Account> members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return groupName != null ? groupName.equals(group.groupName) : group.groupName == null;
    }

    @Override
    public int hashCode() {
        return groupName != null ? groupName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", creator=" + creator.getId() +
                ", members=" + members.size() +
                ", addInfo='" + addInfo + '\'' +
                '}';
    }
}
