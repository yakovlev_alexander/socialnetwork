package com.getjavajob.training.web1608.yakovleva.common;

public enum Relation {
    FRIEND, FOLLOWER, FOLLOWED, STRANGER, ONE_ACCOUNT
}
