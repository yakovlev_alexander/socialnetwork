package com.getjavajob.training.web1608.yakovleva.common.post;

import com.getjavajob.training.web1608.yakovleva.common.Account;
import com.getjavajob.training.web1608.yakovleva.common.SNEntity;

import java.util.Date;

public interface EntityPost<E extends SNEntity> {
    long getId();

    void setId(long id);

    Account getFromAccount();

    void setFromAccount(Account sender);

    E getToEntity();

    void setToEntity(E recipient);

    String getMsgText();

    void setMsgText(String msdText);

    Date getSendingDate();

    void setSendingDate(Date sendingDate);
}
