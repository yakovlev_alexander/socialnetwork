  
# Project "My social"  
  
**Functionality:**  
  
+ registration  
+ AJAX search autocomplete  
+ authentication with Spring Security  
+ AJAX search with pagination  
+ display profile  
+ edit profile  
+ upload profile picture  
+ users export to / import from XML  
+ messaging through WebSocket  
+ posting on wall  
+ groups creation  
+ adding friends  
  
**Tools:**  
JDK 7, Spring 4, JPA 2 / Hibernate 4, XStream, jQuery 2, Twitter Bootstrap 3, Cloudinary, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 7, MySQL / PostgreSQL, IntelliJIDEA 15.  
  
**Notes:**  
MySQL ddl is located in the `db/mysql-ddl.sql`  
PostgreSQL ddl is located in the `db/postgres-ddl.sql`  
  
**Screenshots**
  
![Home page](screenshot/home.jpg)
  
![Messages page](screenshot/messaging.jpg)
  
![Settings page](screenshot/settings.jpg)
  
![Group page](screenshot/group.jpg)
  
_The project hosted at_ https://my-social.herokuapp.com  
  
Test account: ivanov@gmail.com  
Password: qwerty  
__  
Author **_Alexander Yakovlev_**  
Training getJavaJob  
http://www.getjavajob.com
